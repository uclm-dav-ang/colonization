#ifndef INCLUDE_DIAMANTEDATA_H_
#define INCLUDE_DIAMANTEDATA_H_


#include <Ogre.h>
#include <cstdlib>

using namespace Ogre;
using namespace std;


class DiamanteData{

private:
	Vector3 _position;

public:
  DiamanteData (Ogre::Vector3 position);
  ~DiamanteData ();

  Vector3 getPosition() const;
  void setPosition(Vector3 position);
};


#endif
