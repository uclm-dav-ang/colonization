#ifndef INCLUDE_ARMA_H_
#define INCLUDE_ARMA_H_

#include <Ogre.h>
#include <cstdlib>
#include "Enemigo.h"
#include "Robot.h"

using namespace Ogre;
using namespace std;

class Robot;
class Enemigo;
class Arma{

protected:
	SceneNode *_nodeArma;
	Robot *_robot;

	Vector3 _vectorAvance;
	Enemigo *_enemigoObjetivo;

	Ogre::Root *_root;
	Ogre::SceneManager *_sceneMgr;

	int _danio;

	void calcularVectorAvance();
	void avanzar();
	bool hasCollision();
	void quitarVidaEnemigo();
	void deleteArma();
	void ocultar();

	void decrementarContador(Ogre::Real deltaT);

	Ogre::Real _contador;
	Ogre::Timer* _m_precisionTimer;

public:
  Arma(Enemigo* enemigoCercano, String nombre, Robot *robot);
  Arma(const Arma &obj);
  ~Arma();
  Arma& operator= (const Arma &obj);
  SceneNode* getNode() const;
  Enemigo* getEnemigoObjetivo() const;
  Robot* getRobot()const;
  void actualizar(Ogre::Real deltaT);
  Vector3 getVectorAvance();

  Vector3 calcularVectorUnitario(Vector3 vector);
  double calcularModulo(Vector3);

};

#endif
