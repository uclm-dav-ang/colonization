#include <Ogre.h>
#include <cstdlib>
#include <btBulletDynamicsCommon.h>

#include "Enemigo.h"

using namespace Ogre;
using namespace std;

#define ANGULO_CON_ROBOT 20
#define ENEMIGO2X 7.013
#define ENEMIGO2Y 5.499
#define ENEMIGO2Z 7.191

#define PUNTOS2 25;

class Enemigo2 : public Enemigo{

private:
	Ogre::Real _contador;
	Ogre::Timer* _m_precisionTimer;
	bool _comienzoContador;

	bool isNear();
	bool isDetected();

	void andar(Ogre::Real deltaT);
	void perseguir(Ogre::Real deltaT);
	void atacar(Ogre::Real deltaT);

	void decrementarContador(Ogre::Real deltaT);
	void eliminarExplosion();
	void crearExplosion();
	void crearBillboard();
	void morir();

	AnimationState *_animStateAtacar;
	AnimationState *_animStateMorir;

public:
  Enemigo2(String nombre, btVector3 posicion, Robot *robot, btDiscreteDynamicsWorld* world);
  Enemigo2(const Enemigo2 &obj);
  ~Enemigo2();
  Enemigo2& operator= (const Enemigo2 &obj);

  void actuar(Ogre::Real deltaT);
  void restarVida(int danio);

  //estados
  bool _estado_andar;
  bool _estado_perseguir;
  bool _estado_atacar;
};

