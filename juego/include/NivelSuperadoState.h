#ifndef NivelSuperadoState_H
#define NivelSuperadoState_H

#include <Ogre.h>
#include <OIS/OIS.h>

#include "GameState.h"

class NivelSuperadoState : public Ogre::Singleton<NivelSuperadoState>, public GameState
{
 public:
  NivelSuperadoState() {}

  void enter ();
  void exit ();
  void pause ();
  void resume ();

  void keyPressed (const OIS::KeyEvent &e);
  void keyReleased (const OIS::KeyEvent &e);

  void mouseMoved (const OIS::MouseEvent &e);
  void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
  void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

  bool frameStarted (const Ogre::FrameEvent& evt);
  bool frameEnded (const Ogre::FrameEvent& evt);

  // Heredados de Ogre::Singleton.
  static NivelSuperadoState& getSingleton ();
  static NivelSuperadoState* getSingletonPtr ();

 protected:
  Ogre::Root* _root;
  Ogre::SceneManager* _sceneMgr;
  Ogre::Viewport* _viewport;
  Ogre::Camera* _camera;
  Ogre::OverlayManager* _overlayManager;

  bool _exitGame;

};

#endif
