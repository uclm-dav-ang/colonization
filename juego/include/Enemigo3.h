#include <Ogre.h>
#include <cstdlib>
#include <btBulletDynamicsCommon.h>

#include "Enemigo.h"

using namespace Ogre;
using namespace std;

#define ANGULO_CON_ROBOT 20
#define ENEMIGO3X 3.83
#define ENEMIGO3Y 8.108
#define ENEMIGO3Z 8.445

#define PUNTOS3 50;

class Enemigo3 : public Enemigo{

private:
	Ogre::Real _contador;
	Ogre::Timer* _m_precisionTimer;
	bool _comienzoContador;

	bool isNear();
	bool isDetected();

	void andar(Ogre::Real deltaT);
	void perseguir(Ogre::Real deltaT);
	void atacar(Ogre::Real deltaT);

	void decrementarContador(Ogre::Real deltaT);
	void eliminarExplosion();
	void crearExplosion();
	void crearBillboard();
	void morir();

	AnimationState *_animStateAtacar;
	AnimationState *_animStateMorir;

public:
  Enemigo3(String nombre, btVector3 posicion, Robot *robot, btDiscreteDynamicsWorld* world);
  Enemigo3(const Enemigo3 &obj);
  ~Enemigo3();
  Enemigo3& operator= (const Enemigo3 &obj);

  void actuar(Ogre::Real deltaT);
  void restarVida(int danio);

  //estados
  bool _estado_andar;
  bool _estado_perseguir;
  bool _estado_atacar;
};

