#ifndef SCENE_H
#define SCENE_H

#include <vector>
#include <CameraData.h>
#include <CharacterData.h>
#include "EnemigoData.h"
#include "DiamanteData.h"
#include "PilaVidaData.h"
#include "ObjetoArmaData.h"
#include "EnemigoFinalData.h"


class Scene
{
 public:
  Scene ();
  ~Scene ();

  void addCamera (CameraData* camera);
  std::vector<CameraData*> getCameras () const { return _camerasData; }
  CharacterData* getCharacterData () const { return _characterData; }
  void setCharacterData (CharacterData* characterData);
  void addEnemigo(EnemigoData* enemigo);
  void addDiamante(DiamanteData* diamante);
  void addPilaVida(PilaVidaData* pilaVida);
  void addObjetoArma(ObjetoArmaData* objetoArma);
  void setEnemigoFinal(EnemigoFinalData* enemigofinal);
  std::vector<EnemigoData*> getEnemigos () const { return _enemigosData; }
  EnemigoFinalData* getEnemigoFinal () const { return _enemigoFinal; }
  std::vector<DiamanteData*> getDiamantes () const { return _diamantesData; }
  std::vector<PilaVidaData*> getPilaVida () const { return _pilaVidaData; }
  std::vector<ObjetoArmaData*> getObjetoArmaData() const { return _objetoArmaData; }

 private:
  std::vector<CameraData*> _camerasData;
  CharacterData* _characterData;
  std::vector<EnemigoData*> _enemigosData;
  EnemigoFinalData *_enemigoFinal;
  std::vector<DiamanteData*> _diamantesData;
  std::vector<PilaVidaData*> _pilaVidaData;
  std::vector<ObjetoArmaData*> _objetoArmaData;
};

#endif
