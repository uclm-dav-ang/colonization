#ifndef INCLUDE_EXTENDEDCAMERA_H_
#define INCLUDE_EXTENDEDCAMERA_H_

#include <Ogre.h>
#include <cstdlib>
#include <btBulletDynamicsCommon.h>

using namespace Ogre;
using namespace std;


class ExtendedCamera{

private:
	 SceneNode *_mTargetNode; // The camera target
	 SceneNode *_mCameraNode; // The camera itself
	 Camera *_mCamera; // Ogre camera

	 SceneManager *_mSceneMgr;
	 String _mName;

	 bool _mOwnCamera; // To know if the ogre camera binded has been created outside or inside of this class

	 Real _mTightness; // Determines the movement of the camera - 1 means tight movement, while 0 means no movement

public:
  ExtendedCamera(String name, SceneManager *sceneMgr, Camera *camera);
  ~ExtendedCamera();
  void setTightness (Real tightness);
  Real getTightness();
  Vector3 getCameraPosition();
  void instantUpdate(Vector3 cameraPosition, Vector3 targetPosition);
  void update(Real elapsedTime, Vector3 cameraPosition, Vector3 targetPosition);
};



#endif
