/* **********************************************************
** Importador NoEscape 1.0
** Curso de Experto en Desarrollo de Videojuegos 
** Escuela Superior de Informatica - Univ. Castilla-La Mancha
** Carlos Gonzalez Morcillo - David Vallejo Fernandez
************************************************************/

#ifndef IMPORTER_H
#define IMPORTER_H

#include <OGRE/Ogre.h>
#include <xercesc/dom/DOM.hpp>

#include "CharacterData.h"
#include "Scene.h"

using namespace Ogre;

class Importer: public Ogre::Singleton<Importer> {
 public:
  void parseScene (const char* path, Scene* scene);
  
  static Importer& getSingleton ();
  static Importer* getSingletonPtr ();
  
 private:
  void parseCamera (xercesc::DOMNode* cameraNode, Scene* scene);
  
  void parseCharacter (xercesc::DOMNode* characterNode, Scene* scene);
  void parseEnemigo (xercesc::DOMNode* enemigoNode, Scene* scene);
  void parseDiamante (xercesc::DOMNode* diamanteNode, Scene* scene);
  void parsePilaVida (xercesc::DOMNode* pilaVidaNode, Scene* scene);
  void parseObjetoArma (xercesc::DOMNode* objetoArmaNode, Scene* scene);
  void getVector(xercesc::DOMNode* node, Vector3* position);
  float getValueFromTag (xercesc::DOMNode* node, const XMLCh *tag);
};

#endif
