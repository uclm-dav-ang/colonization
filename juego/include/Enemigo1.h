#include <Ogre.h>
#include <cstdlib>
#include <btBulletDynamicsCommon.h>

#include "Enemigo.h"

using namespace Ogre;
using namespace std;

#define ANGULO_CON_ROBOT 20
#define ENEMIGO1X 2.769
#define ENEMIGO1Y 4.611
#define ENEMIGO1Z 1.32

#define PUNTOS1 10;

class Enemigo1 : public Enemigo{

private:
	Ogre::Real _contador;
	Ogre::Timer* _m_precisionTimer;
	bool _comienzoContador;

	bool isNear();
	bool isDetected();

	void andar(Ogre::Real deltaT);
	void perseguir(Ogre::Real deltaT);
	void atacar();

	void decrementarContador(Ogre::Real deltaT);
	void eliminarExplosion();
	void crearExplosion();
	void morir();

public:
  Enemigo1(String nombre, btVector3 posicion, Robot *robot, btDiscreteDynamicsWorld* world);
  Enemigo1(const Enemigo1 &obj);
  ~Enemigo1();
  Enemigo1& operator= (const Enemigo1 &obj);

  void actuar(Ogre::Real deltaT);
  void restarVida(int danio);

  //estados
  bool _estado_andar;
  bool _estado_perseguir;
  bool _estado_atacar;
};

