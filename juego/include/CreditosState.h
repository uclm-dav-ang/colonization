#ifndef INCLUDE_CREDITOSTATE_H_
#define INCLUDE_CREDITOSTATE_H_

#include <Ogre.h>
#include <OIS/OIS.h>

#include "GameState.h"

class CreditosState : public Ogre::Singleton<CreditosState>, public GameState
{
 public:
  CreditosState() {}

  void enter ();
  void exit ();
  void pause ();
  void resume ();

  void keyPressed (const OIS::KeyEvent &e);
  void keyReleased (const OIS::KeyEvent &e);

  void mouseMoved (const OIS::MouseEvent &e);
  void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
  void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

  bool frameStarted (const Ogre::FrameEvent& evt);
  bool frameEnded (const Ogre::FrameEvent& evt);

  // Heredados de Ogre::Singleton.
  static CreditosState& getSingleton ();
  static CreditosState* getSingletonPtr ();

 protected:
  Ogre::Root* _root;
  Ogre::SceneManager* _sceneMgr;
  Ogre::Viewport* _viewport;
  Ogre::Camera* _camera;
  Ogre::OverlayManager* _overlayManager;

  bool _exitGame;

 private:

};

#endif
