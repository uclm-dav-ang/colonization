#ifndef EnemigoFinalData_H
#define EnemigoFinalData_H

#include <Ogre.h>
#include <cstdlib>
#include <btBulletDynamicsCommon.h>

using namespace Ogre;
using namespace std;


class EnemigoFinalData{

private:
	Vector3 _position;
	int _type;

public:
  EnemigoFinalData (Ogre::Vector3 position, int type);
  ~EnemigoFinalData ();

  Vector3 getPosition() const;
  void setPosition(Vector3 position);

  int getType() const;
  void setType(int type);
};

#endif
