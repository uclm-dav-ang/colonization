#ifndef INCLUDE_ARMA1_H_
#define INCLUDE_ARMA1_H_

#include  "Arma.h"

using namespace Ogre;
using namespace std;

class Arma1 : public Arma{

public:
  Arma1(Enemigo* enemigoCercano, String nombre, Robot *robot);
  Arma1(const Arma1 &obj);
  ~Arma1();
  Arma1& operator= (const Arma1 &obj);
};

#endif
