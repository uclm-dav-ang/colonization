#ifndef INCLUDE_OBJETOARMA_H_
#define INCLUDE_OBJETOARMA_H_

#include <Ogre.h>
#include <cstdlib>
#include <btBulletDynamicsCommon.h>

using namespace Ogre;
using namespace std;


class ObjetoArma{

private:
	SceneNode *_nodeObjetoArma;

	AnimationState *_animStateRotarObjetoArma;

	void configurarAnimacion(Ogre::AnimationState *animstate, bool loop);
	void addTimeAnimacion(Ogre::AnimationState *animState, Ogre::Real deltaT);
	void pararAnimacion(Ogre::AnimationState *animState);

public:
	ObjetoArma(SceneNode* nodeObjetoArma);
	ObjetoArma(const ObjetoArma &obj);
	~ObjetoArma();
	ObjetoArma& operator= (const ObjetoArma &obj);
	SceneNode* getNode() const;
	void ocultar();

	void animar(Ogre::Real deltaT);

};

#endif /* INCLUDE_OBJETOARMA_H_ */
