#ifndef INCLUDE_OBJETOARMADATA_H_
#define INCLUDE_OBJETOARMADATA_H_


#include <Ogre.h>
#include <cstdlib>

using namespace Ogre;
using namespace std;


class ObjetoArmaData{

private:
	Vector3 _position;

public:
	ObjetoArmaData (Ogre::Vector3 position);
  ~ObjetoArmaData ();

  Vector3 getPosition() const;
  void setPosition(Vector3 position);
};


#endif
