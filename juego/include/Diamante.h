/*
 * Diamante.h
 *
 *  Created on: 23/6/2015
 *      Author: angel
 */

#ifndef INCLUDE_DIAMANTE_H_
#define INCLUDE_DIAMANTE_H_

#include <Ogre.h>
#include <cstdlib>
#include <btBulletDynamicsCommon.h>

using namespace Ogre;
using namespace std;


class Diamante{

private:
	SceneNode *_nodeDiamante;

	AnimationState *_animStateRotarDiamante;

	void configurarAnimacion(Ogre::AnimationState *animstate, bool loop);
	void addTimeAnimacion(Ogre::AnimationState *animState, Ogre::Real deltaT);
	void pararAnimacion(Ogre::AnimationState *animState);

public:
  Diamante(SceneNode* nodeDiamante);
  Diamante(const Diamante &obj);
  ~Diamante();
  Diamante& operator= (const Diamante &obj);
  SceneNode* getNode() const;
  void ocultar();

  void animar(Ogre::Real deltaT);

};

#endif /* INCLUDE_DIAMANTE_H_ */
