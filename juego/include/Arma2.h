#ifndef INCLUDE_ARMA2_H_
#define INCLUDE_ARMA2_H_

#include  "Arma.h"

using namespace Ogre;
using namespace std;

class Arma2 : public Arma{

public:
  Arma2(Enemigo* enemigoCercano, String nombre, Robot *robot);
  Arma2(const Arma2 &obj);
  ~Arma2();
  Arma2& operator= (const Arma2 &obj);
};

#endif
