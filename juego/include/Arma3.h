#ifndef INCLUDE_ARMA3_H_
#define INCLUDE_ARMA3_H_

#include  "Arma.h"

using namespace Ogre;
using namespace std;

class Arma3 : public Arma{

public:
  Arma3(Enemigo* enemigoCercano, String nombre, Robot *robot);
  Arma3(const Arma3 &obj);
  ~Arma3();
  Arma3& operator= (const Arma3 &obj);
};

#endif
