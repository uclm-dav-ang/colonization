#ifndef CAMERAData_H
#define CAMERAData_H

#include <iostream>
#include <vector>
#include "Ogre.h"

class CameraData
{
 public:
  CameraData (Ogre::Vector3 position, Ogre::Vector3 lookAt);
  ~CameraData ();

  Ogre::Vector3 getPosition();
  Ogre::Vector3 getLookAt();

 private:
  Ogre::Vector3 _position;
  Ogre::Vector3 _lookAt;
};

#endif
