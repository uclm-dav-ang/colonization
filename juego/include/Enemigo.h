#ifndef Enemigo_H
#define Enemigo_H

#include <Ogre.h>
#include <cstdlib>
#include <btBulletDynamicsCommon.h>
#include "MyMotionState.h"


#include "Robot.h"

using namespace Ogre;
using namespace std;

#define ANGULO_CON_ROBOT 20
#define TIEMPO_ANDAR 3000
#define TIEMPO_GIRAR 500

class Robot;
class Enemigo{

protected:
	GameManager* _gameManager;

	String _mName;
	double _centro;
	btVector3 _initialPosition;
	SceneNode *_nodeEnemigo;
	Entity *_entEnemigo;
	SceneNode *_nodeEnemigoAvance;
	btCollisionShape* _EnemigoShape;
	btRigidBody* _EnemigoRigidBody;
	bool _isDead;
	btDiscreteDynamicsWorld* _world;

	Ogre::Real _contadorAndar;
	Ogre::Timer* _m_precisionTimerAndar;

	Robot *_robot;
	btVector3 _linearVelocity;
	bool _giroIzquierda;

	Ogre::Root *_root;
	Ogre::SceneManager *_sceneMgr;

	Ogre::AnimationState *_animStateAndar;

	int _vida, _puntos;

	void calcularLinearVelocity();
	void girar();
	void pararGiro();
	void pararAvance();
	void quitarVidaRobot();

	void avanceAndar(Ogre::Real deltaT);

	void configurarAnimacion(Ogre::AnimationState *animstate, bool loop);
	void addTimeAnimacion(Ogre::AnimationState *animState, Ogre::Real deltaT);
	void pararAnimacion(Ogre::AnimationState *animState);

	void deleteEnemigo();
	void ocultar();
	void addPuntos(int puntos);
	void reproducirGolpe();

public:
  Enemigo(String nombre, btVector3 posicion, Robot *robot, btDiscreteDynamicsWorld* world);
  Enemigo(const Enemigo &obj);
  virtual ~Enemigo();
  Enemigo& operator= (const Enemigo &obj);
  SceneNode* getNode() const;
  SceneNode* getNodeAvance() const;
  btCollisionShape* getShape() const;
  btRigidBody* getRigidBody() const;
  Robot* getRobot() const;

  virtual void actuar(Ogre::Real deltaT) = 0;
  bool isDead();

  btVector3 calcularVectorRobot();
  double calcularModulo(btVector3);
  double calcularAngulo(btVector3 vector1, btVector3 vector2);
  virtual void restarVida(int danio) = 0;

  int _poder_ataque;
};

#endif
