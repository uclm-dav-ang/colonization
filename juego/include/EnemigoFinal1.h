#include <Ogre.h>
#include <cstdlib>
#include <btBulletDynamicsCommon.h>

#include "Enemigo.h"

using namespace Ogre;
using namespace std;

#define ANGULO_CON_ROBOT 20

class EnemigoFinal1 : public Enemigo{

private:
	Ogre::Real _contador;
	Ogre::Timer* _m_precisionTimer;
	bool _comienzoContador;

	bool isNear();
	bool isDetected();

	void andar();
	void perseguir(Ogre::Real deltaT);
	void atacar();

	void decrementarContador(Ogre::Real deltaT);
	void eliminarExplosion();
	void crearExplosion();
	void crearBillboard();
	void morir();

public:
  EnemigoFinal1(String nombre, btVector3 posicion, Robot *robot, btDiscreteDynamicsWorld* world);
  EnemigoFinal1(const EnemigoFinal1 &obj);
  ~EnemigoFinal1();
  EnemigoFinal1& operator= (const EnemigoFinal1 &obj);

  void actuar(Ogre::Real deltaT);
  void restarVida(int danio);

  //estados
  bool _estado_andar;
  bool _estado_perseguir;
  bool _estado_atacar;
};

