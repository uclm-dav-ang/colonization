#ifndef INCLUDE_PILAVIDADATA_H_
#define INCLUDE_PILAVIDADATA_H_


#include <Ogre.h>
#include <cstdlib>

using namespace Ogre;
using namespace std;


class PilaVidaData{

private:
	Vector3 _position;

public:
	PilaVidaData (Ogre::Vector3 position);
  ~PilaVidaData ();

  Vector3 getPosition() const;
  void setPosition(Vector3 position);
};


#endif
