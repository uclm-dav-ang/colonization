#ifndef ElegirNivelState_H
#define ElegirNivelState_H

#include <Ogre.h>
#include <OIS/OIS.h>

#include "GameState.h"

#define NUMERO_NIVELES 3

class ElegirNivelState : public Ogre::Singleton<ElegirNivelState>, public GameState
{
 public:
  ElegirNivelState() {}

  void enter ();
  void exit ();
  void pause ();
  void resume ();

  void keyPressed (const OIS::KeyEvent &e);
  void keyReleased (const OIS::KeyEvent &e);

  void mouseMoved (const OIS::MouseEvent &e);
  void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
  void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

  bool frameStarted (const Ogre::FrameEvent& evt);
  bool frameEnded (const Ogre::FrameEvent& evt);

  void crearMenu();
  void inicializarAnimaciones();

  void setNivelConseguido(int numeroNivel);
  void setNivelesConseguidos(std::vector<bool> nivelesConsequidos);

  // Heredados de Ogre::Singleton.
  static ElegirNivelState& getSingleton ();
  static ElegirNivelState* getSingletonPtr ();

 protected:
  Ogre::Root* _root;
  Ogre::SceneManager* _sceneMgr;
  Ogre::Viewport* _viewport;
  Ogre::Camera* _camera;

  bool _exitGame;

 private:
  int _menuSeleccion;
  std::vector<Ogre::SceneNode*> _planetas;
  std::vector<Ogre::AnimationState*> _animacionPlanetas;
  std::vector<bool> _nivelesConseguidos;

  void cambiarTexturaMenu();
};

#endif
