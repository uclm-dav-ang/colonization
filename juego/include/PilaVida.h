#ifndef INCLUDE_PILAVIDA_H_
#define INCLUDE_PILAVIDA_H_

#include <Ogre.h>
#include <cstdlib>
#include <btBulletDynamicsCommon.h>

#include "GameManager.h"

using namespace Ogre;
using namespace std;


class PilaVida{

private:
	SceneNode *_nodePilaVida;
	int _vida;

	AnimationState *_animStateRotarPila;

	void configurarAnimacion(Ogre::AnimationState *animstate, bool loop);
	void addTimeAnimacion(Ogre::AnimationState *animState, Ogre::Real deltaT);
	void pararAnimacion(Ogre::AnimationState *animState);

public:
	PilaVida(SceneNode* nodeDiamante);
  PilaVida(const PilaVida &obj);
  ~PilaVida();
  PilaVida& operator= (const PilaVida &obj);
  SceneNode* getNode() const;
  void ocultar();
  int getVida();

  void animar(Ogre::Real deltaT);

};

#endif /* INCLUDE_PILAVIDA_H_ */
