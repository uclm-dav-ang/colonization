#ifndef Robot_H
#define Robot_H

#include <Ogre.h>
#include <cstdlib>
#include <btBulletDynamicsCommon.h>

#include "Diamante.h"
#include "PilaVida.h"
#include "ObjetoArma.h"
#include "Arma.h"

using namespace Ogre;
using namespace std;

#define ROBOT_ALTURA 7.215

class Enemigo;
class Arma;
class Robot{

private:
	GameManager* _gameManager;

	Ogre::Root *_root;
	Ogre::SceneManager *_sceneMgr;
	Ogre::OverlayManager* _overlayManager;

	SceneNode *_nodeRobot;
	SceneNode *_nodeRobotAvance;
	SceneNode *_mSightNode;
	SceneNode *_mCameraNode;

	btCollisionShape* _robotShape;
	btRigidBody* _robotRigidBody;

	double _mVerticalVelocity;
	btVector3 _linearVelocity;
	btVector3 _vectorDireccion;
	bool _onGround;

	bool _isFalling;
	double _verticalLastPosition;
	double _verticalCurrentPosition;

	std::vector<Enemigo*> _enemigos;
	std::vector<Enemigo*> _enemigosCercanos;
	Enemigo* _enemigoObjetivo;

	std::vector<Arma*> _armas;
	std::vector<Diamante*> _diamantes;
	std::vector<PilaVida*> _pilasVida;
	std::vector<ObjetoArma*> _objetoArmas;
	int _contadorArmas;

	String _mName;
	AnimationState *_animStateDesplazarse;
	AnimationState *_animStateDisparo1;
	AnimationState *_animStateDisparo1Volver;
	AnimationState *_animStateMorir;

	//movimiento
	bool _avanzar;
	bool _girarIzquierda;
	bool _girarDerecha;
	bool _estaDisparando;
	bool _saltar;
	bool _estaEnAire;

	std::vector<bool> _armasConseguidas;
	std::vector<bool> _nivelesConseguidos;

	int _armaElegida;

	int _vida;
	int _nivel;
	int _puntos;

	bool _hasPerdido;
	bool hasCaido();

	void configurarAnimacion(Ogre::AnimationState *animstate, bool loop);
	void addTimeAnimacion(Ogre::AnimationState *animState, Ogre::Real deltaT);
	void pararAnimacion(Ogre::AnimationState *animState);
	void checkAnimacionDisparo(Ogre::Real deltaT);

	void calcularEnemigoObjetivo();
	void calcularEnemigosCercanos();
	double calcularVerticalVelocity();

	void ajustarVida();
	void mostrarOverlayArma();

	void morir();

public:
  Robot(SceneNode* nodeRobot, SceneNode* nodeRobotAvance, btCollisionShape* robotShape, btRigidBody* robotRigidBody, int nivel);
  Robot(const Robot &obj);
  ~Robot();
  Robot& operator= (const Robot &obj);
  SceneNode* getNode() const;
  SceneNode* getNodeAvance() const;
  SceneNode* getSightNode() const;
  SceneNode* getCameraNode() const;
  Vector3 getWorldPosition();

  btCollisionShape* getShape() const;
  btRigidBody* getRigidBody() const;
  btVector3 getLinearVelocity();
  btVector3 getVectorDireccion();

  bool isFalling();

  void addEnemigo(Enemigo* enemigo);
  void addArma(Arma* arma);
  void addDiamante(Diamante* diamante);
  void addPilaVida(PilaVida* pilaVida);
  void addObjetoArma(ObjetoArma* objetoArma);

  void setEnemigos(std::vector<Enemigo*> enemigos);
  std::vector<Enemigo*> getEnemigos();
  void setArmas(std::vector<Arma*> armas);
  std::vector<Arma*> getArmas();
  std::vector<bool> getArmasConseguidas();
  void setDiamantes(std::vector<Diamante*> diamantes);
  std::vector<Diamante*> getDiamantes();
  void setPilaVida(std::vector<PilaVida*> pilaVida);
  std::vector<PilaVida*> getPilaVida();
  void setObjetoArma(std::vector<ObjetoArma*> objetoArma);
  std::vector<ObjetoArma*> getObjetoArma();

  void calcularLinearVelocity();
  void actuar(Ogre::Real deltaT);
  void avanzar(Ogre::Real deltaT);
  void girarIzquierda();
  void girarDerecha();
  void pararAvance();
  void pararGiro();
  void setVida(int vida);
  int getVida();

  btVector3 calcularVectorEnemigo(Enemigo* enemigo);
  double calcularModulo(btVector3);
  double calcularAngulo(btVector3 vector1, btVector3 vector2);

  void disparar();
  void actualizarDisparos(Ogre::Real deltaT);

  void setOnGround(bool onGround);
  bool isOnGround();
  bool estaEnAire();
  bool getSaltar();
  int getArmaSeleccionada();

  bool conseguirObjeto(Ogre::Real deltaT);
  void conseguirPilaVida(Ogre::Real deltaT);
  void conseguirArma(Ogre::Real deltaT);

  void setAvanzar(bool activado);
  void setGirarIzquierda(bool activado);
  void setGirarDerecha(bool activado);
  void setEstaDisparando(bool activado);
  void setSaltar(bool activado);

  void setArmaConseguida(int numeroArma);
  void setArmasConseguidas(std::vector<bool> armasConsequidas);
  void setArmaSeleccionada(int numeroArma);

  void addPuntos(int puntos);
  int getPuntos();
  void setPuntos(int puntos);

  bool isDead();
  bool hasPerdido();
};

#endif
