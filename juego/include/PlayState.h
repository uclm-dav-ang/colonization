#ifndef PlayState_H
#define PlayState_H

#include <Ogre.h>
#include <OIS/OIS.h>
#include <btBulletDynamicsCommon.h>

#include "Robot.h"
#include "Enemigo1.h"
#include "Enemigo2.h"
#include "Enemigo3.h"
#include "Enemigo4.h"
#include "EnemigoFinal1.h"
#include "Diamante.h"
#include "PilaVida.h"
#include "ObjetoArma.h"
#include "Scene.h"
#include "GameState.h"
#include "ExtendedCamera.h"

using namespace std;
using namespace Ogre;


class PlayState : public Ogre::Singleton<PlayState>, public GameState
{
 public:
  PlayState () {}

  void enter ();
  void exit ();
  void pause ();
  void resume ();

  void keyPressed (const OIS::KeyEvent &e);
  void keyReleased (const OIS::KeyEvent &e);

  void mouseMoved (const OIS::MouseEvent &e);
  void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
  void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

  bool frameStarted (const Ogre::FrameEvent& evt);
  bool frameEnded (const Ogre::FrameEvent& evt);

  // Heredados de Ogre::Singleton.
  static PlayState& getSingleton ();
  static PlayState* getSingletonPtr ();


  int getPuntuacion();
  int getNivel();
  void setPuntos(int puntos);
  void setNivel(int nivel);
  void comprobarEnemigoFinal();

  void mostrarOverlayArmasConseguidas();
  void mostrarOverlayArmaConseguidaSeleccionada1();
  void mostrarOverlayArmaConseguidaSeleccionada2();
  void mostrarOverlayArmaConseguidaSeleccionada3();

 protected:
  Ogre::Root* _root;
  Ogre::SceneManager* _sceneMgr;
  Ogre::Viewport* _viewport;
  Ogre::Camera* _camera;

  bool _exitGame;

 private:
  int _nivel;

  std::vector<char> _vector_nombre;
  char _nombre[20];

  Ogre::OverlayManager* _overlayManager;
  Ogre::AnimationState *_aimState;

  btBroadphaseInterface* _broadphase;
  btDefaultCollisionConfiguration* _collisionConf;
  btCollisionDispatcher* _dispatcher;
  btSequentialImpulseConstraintSolver* _solver;
  btDiscreteDynamicsWorld* _world;

  btCollisionShape* _groundShape;

  Ogre::SceneNode* _node_principal;
  Scene *_scene;
  Robot *_robot;
  std::vector<bool> _armasConseguidas;
  int _puntos;

  int _contadorEnemigos;
  int _contadorDiamantes;
  int _diamantesConseguidos;
  int _contadorPilaVida;
  int _contadorObjetoArma;
  bool _mover, _girarIzquierda, _girarDerecha;

  int _diamantesTotales;

  bool _allEnemiesDead;
  Enemigo *_enemigoFinal;

  ExtendedCamera *_exCamera;

  Ogre::Overlay* _overlayArmaSeleccionada1;
  Ogre::Overlay* _overlayArmaSeleccionada2;
  Ogre::Overlay* _overlayArmaSeleccionada3;

  void CreateInitialWorld();
  void crearLuz();
  void crearRobot(Ogre::Vector3 position);
  void crearNodosRobot(Ogre::Vector3 position);
  void crearEnemigo(Ogre::Vector3 position, int type);
  void crearTerreno();
  void crearDiamantes(Ogre::Vector3 position);
  void crearPilaVida(Ogre::Vector3 position);
  void crearObjetoArma(Ogre::Vector3 position);
  void parsear_escenario();

  void checkOnGround();

  void enemigoFinal();
  void comprobarNivelTerminado();
  void comprobarDerrota();
};

#endif
