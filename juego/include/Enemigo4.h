#include <Ogre.h>
#include <cstdlib>
#include <btBulletDynamicsCommon.h>

#include "Enemigo.h"

using namespace Ogre;
using namespace std;

#define ANGULO_CON_ROBOT 20
#define ENEMIGO4X 4.778
#define ENEMIGO4Y 7.405
#define ENEMIGO4Z 7.383

#define PUNTOS4 100;

class Enemigo4 : public Enemigo{

private:
	Ogre::Real _contador;
	Ogre::Timer* _m_precisionTimer;
	bool _comienzoContador;

	bool isNear();
	bool isDetected();

	void andar(Ogre::Real deltaT);
	void perseguir(Ogre::Real deltaT);
	void atacar(Ogre::Real deltaT);

	void decrementarContador(Ogre::Real deltaT);
	void eliminarExplosion();
	void crearExplosion();
	void crearBillboard();
	void morir();

	AnimationState *_animStateAtacar;
	AnimationState *_animStateMorir;

public:
  Enemigo4(String nombre, btVector3 posicion, Robot *robot, btDiscreteDynamicsWorld* world);
  Enemigo4(const Enemigo4 &obj);
  ~Enemigo4();
  Enemigo4& operator= (const Enemigo4 &obj);

  void actuar(Ogre::Real deltaT);
  void restarVida(int danio);

  //estados
  bool _estado_andar;
  bool _estado_perseguir;
  bool _estado_atacar;
};

