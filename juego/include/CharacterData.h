#ifndef CharacterData_H
#define CharacterData_H

#include <Ogre.h>
#include <cstdlib>
#include <btBulletDynamicsCommon.h>

#include "GameManager.h"

using namespace Ogre;
using namespace std;


class CharacterData{

private:
	Vector3 _position;

public:
  Vector3 getPosition() const;
  void setPosition(Vector3 position);
};

#endif
