#ifndef EnemigoData_H
#define EnemigoData_H

#include <Ogre.h>
#include <cstdlib>
#include <btBulletDynamicsCommon.h>

using namespace Ogre;
using namespace std;


class EnemigoData{

private:
	Vector3 _position;
	int _type;

public:
  EnemigoData (Ogre::Vector3 position, int type);
  ~EnemigoData ();

  Vector3 getPosition() const;
  void setPosition(Vector3 position);

  int getType() const;
  void setType(int type);
};

#endif
