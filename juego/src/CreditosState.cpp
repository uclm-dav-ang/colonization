#include "../include/CreditosState.h"

#include "IntroState.h"
#include "PlayState.h"

template<> CreditosState* Ogre::Singleton<CreditosState>::msSingleton = 0;

void CreditosState::enter() {
	_root = Ogre::Root::getSingletonPtr();

	// Se recupera el gestor de escena y la cámara.
	_sceneMgr = _root->getSceneManager("SceneManager");
	_camera = _sceneMgr->getCamera("IntroCamera");
	_viewport = _root->getAutoCreatedWindow()->addViewport(_camera);

	Ogre::SceneNode *node_menu = _sceneMgr->createSceneNode("Menu");
	_sceneMgr->getRootSceneNode()->addChild(node_menu);

	//Nodo PlanoMenu
	Ogre::SceneNode *node_PlanoMenu = _sceneMgr->createSceneNode("PlanoMenu");
	Ogre::Entity* ent_PlanoMenu = _sceneMgr->createEntity("PlanoMenu", "PlanoMenu.mesh");
	node_PlanoMenu->attachObject(ent_PlanoMenu);
	node_menu->addChild(node_PlanoMenu);
	node_PlanoMenu->translate(0, 12, 0);
	node_PlanoMenu->yaw(Degree(90));

	//mostramos el overlay
	_overlayManager = Ogre::OverlayManager::getSingletonPtr();
	Ogre::Overlay *overlay = _overlayManager->getByName("Creditos");
	overlay->show();
	Ogre::OverlayElement *oe;
	oe = _overlayManager->getOverlayElement("Nombre1");
	oe->setCaption("Angel Luis Sanchez Gomez");
	oe = _overlayManager->getOverlayElement("Nombre2");
	oe->setCaption("David Valencia Delgado-Corredor");

	_exitGame = false;
}

void CreditosState::exit() {
	_root->getAutoCreatedWindow()->removeAllViewports();
	_sceneMgr->destroyAllCameras();
	_sceneMgr->clearScene();
	_root->destroySceneManager(_sceneMgr);
	Ogre::OverlayManager::getSingletonPtr()->getByName("Creditos")->hide();
}

void CreditosState::pause() {
}

void CreditosState::resume() {
}

bool CreditosState::frameStarted(const Ogre::FrameEvent& evt) {
	return true;
}

bool CreditosState::frameEnded(const Ogre::FrameEvent& evt) {
	if (_exitGame)
		return false;

	return true;
}

void CreditosState::keyPressed(const OIS::KeyEvent &e) {

}

void CreditosState::keyReleased(const OIS::KeyEvent &e) {
	 if (e.key == OIS::KC_ESCAPE) {
		_exitGame = true;
		 changeState(IntroState::getSingletonPtr());
	  }
}

void CreditosState::mouseMoved(const OIS::MouseEvent &e) {
}

void CreditosState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id) {
}

void CreditosState::mouseReleased(const OIS::MouseEvent &e,
		OIS::MouseButtonID id) {
}

CreditosState*
CreditosState::getSingletonPtr() {
	return msSingleton;
}

CreditosState&
CreditosState::getSingleton() {
	assert(msSingleton);
	return *msSingleton;
}
