#include <PilaVidaData.h>
#include <OgreSceneNode.h>
#include <cmath>

PilaVidaData::PilaVidaData
(Ogre::Vector3 position)
{
	_position = position;
}

PilaVidaData::~PilaVidaData ()
{
}

Vector3 PilaVidaData::getPosition()const {
	return _position;
}

void PilaVidaData::setPosition(Vector3 position){
	_position = position;
}
