#include "../include/CameraData.h"

CameraData::CameraData
(Ogre::Vector3 position, Ogre::Vector3 lookAt)
{
	_position = position;
	_lookAt = lookAt;
}

CameraData::~CameraData ()
{
}

Ogre::Vector3 CameraData::getPosition(){
	return _position;
}

Ogre::Vector3 CameraData::getLookAt(){
	return _lookAt;
}
