#include "ExtendedCamera.h"

ExtendedCamera::ExtendedCamera (String name, SceneManager *sceneMgr, Camera *camera = 0) {
	// Basic member references setup
	_mName = name;
	_mSceneMgr = sceneMgr;

	// Create the camera's node structure
	_mCameraNode = _mSceneMgr->getRootSceneNode ()->createChildSceneNode (_mName);
	_mTargetNode = _mSceneMgr->getRootSceneNode ()->createChildSceneNode (_mName + "_target");
	_mCameraNode->setAutoTracking (true, _mTargetNode); // The camera will always look at the camera target
	_mCameraNode->setFixedYawAxis(true); // Needed because of auto tracking

	// Create our camera if it wasn't passed as a parameter
	if (camera == 0) {
		_mCamera = _mSceneMgr->createCamera (_mName);
		_mOwnCamera = true;
	}
	else {
		_mCamera = camera;
		// just to make sure that mCamera is set to 'origin' (same position as the mCameraNode)
		_mCamera->setPosition(0.0,0.0,0.0);
		_mOwnCamera = false;
	}
	// ... and attach the Ogre camera to the camera node
	_mCameraNode->attachObject (_mCamera);
	_mCamera->roll(Degree(90));

	// Default tightness
	_mTightness = 0.02f;
}
ExtendedCamera::~ExtendedCamera () {
	_mCameraNode->detachAllObjects ();
	if (_mOwnCamera)
		delete _mCamera;
	_mSceneMgr->destroySceneNode (_mName);
	_mSceneMgr->destroySceneNode (_mName + "_target");
}

void ExtendedCamera::setTightness (Real tightness) {
	_mTightness = tightness;
}

Real ExtendedCamera::getTightness () {
	return _mTightness;
}

Vector3 ExtendedCamera::getCameraPosition () {
	return _mCameraNode->getPosition ();
}

void ExtendedCamera::instantUpdate (Vector3 cameraPosition, Vector3 targetPosition) {
	_mCameraNode->setPosition (cameraPosition);
	_mTargetNode->setPosition (targetPosition);
}

void ExtendedCamera::update (Real elapsedTime, Vector3 cameraPosition, Vector3 targetPosition) {
	// Handle movement
	Vector3 displacement;

	displacement = (cameraPosition - _mCameraNode->getPosition ()) * _mTightness;
	_mCameraNode->translate (displacement);

	displacement = (targetPosition - _mTargetNode->getPosition ()) * _mTightness;
	_mTargetNode->translate (displacement);
}
