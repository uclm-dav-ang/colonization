#include <CharacterData.h>
#include <OgreSceneNode.h>
#include <cmath>

Vector3 CharacterData::getPosition()const {
	return _position;
}

void CharacterData::setPosition(Vector3 position){
	_position = position;
}
