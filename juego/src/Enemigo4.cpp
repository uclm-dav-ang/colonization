#include <Enemigo4.h>
#include <OgreSceneNode.h>
#include <cmath>

Enemigo4::Enemigo4(String nombre, btVector3 posicion, Robot *robot, btDiscreteDynamicsWorld* world) : Enemigo( nombre,  posicion, robot,  world) {
	_centro = ENEMIGO4Y/2;

	//nodos y rigidBody
	_EnemigoShape = new btBoxShape(btVector3(ENEMIGO4X/2, ENEMIGO4Y/2, ENEMIGO4Z/2));
	btVector3 fallInertia(0, 0, 0);

	btScalar enemigoMass = 10;

	_nodeEnemigo = _sceneMgr->createSceneNode(_mName);
	_entEnemigo = _sceneMgr->createEntity(_mName, "Lagarto.mesh");
	_nodeEnemigo->attachObject(_entEnemigo);
	_entEnemigo->getMesh()->buildEdgeList();
	_sceneMgr->getRootSceneNode()->addChild(_nodeEnemigo);
	MyMotionState* MotionStateEnemigo = new MyMotionState(btTransform(btQuaternion(0, 0, 0, 1), posicion), _nodeEnemigo);
	_EnemigoShape->calculateLocalInertia(enemigoMass, fallInertia);
	btRigidBody::btRigidBodyConstructionInfo EnemigoRigidBodyCI(enemigoMass, MotionStateEnemigo, _EnemigoShape, fallInertia);
	_EnemigoRigidBody = new btRigidBody(EnemigoRigidBodyCI);
	_EnemigoRigidBody->setRollingFriction(0);
	_EnemigoRigidBody->setFriction(0);
	_EnemigoRigidBody->setAngularFactor(0);
	_world->addRigidBody(_EnemigoRigidBody);

	stringstream nombreAvance;
	nombreAvance << _mName << "avance";

	_nodeEnemigoAvance = _sceneMgr->createSceneNode(nombreAvance.str());
	_nodeEnemigo->addChild(_nodeEnemigoAvance);
	_nodeEnemigoAvance->translate(0.0, 0.0, 1.0);

	_estado_andar = true;
	_estado_atacar = false;
	_estado_perseguir = false;

	_giroIzquierda = true;

	_poder_ataque = 25;
	_vida = 400;
	_puntos = PUNTOS4;


	_animStateAndar = _entEnemigo->getAnimationState("Andar");
	_animStateAtacar = _entEnemigo->getAnimationState("Atacar");
	_animStateMorir = _entEnemigo->getAnimationState("Morir");
}

Enemigo4::Enemigo4(const Enemigo4 &obj) : Enemigo (obj){
	_nodeEnemigo = obj.getNode();
	_nodeEnemigoAvance = obj.getNodeAvance();
	_EnemigoShape = obj.getShape();
	_EnemigoRigidBody = obj.getRigidBody();
	_robot = obj.getRobot();
}

Enemigo4::~Enemigo4() {
	delete _nodeEnemigo;
	delete _nodeEnemigoAvance;
	delete _EnemigoShape;
	delete _EnemigoRigidBody;
}

Enemigo4& Enemigo4::operator= (const Enemigo4 &obj){
	delete _nodeEnemigo;
	delete _nodeEnemigoAvance;
	delete _EnemigoShape;
	delete _EnemigoRigidBody;
	_nodeEnemigo = obj._nodeEnemigo;
	_nodeEnemigo =obj._nodeEnemigoAvance;
	_EnemigoShape = obj._EnemigoShape;
	_EnemigoRigidBody = obj._EnemigoRigidBody;
	return *this;
}

void Enemigo4::actuar(Ogre::Real deltaT){
	if(!_isDead){
		if (_estado_andar) {
			andar(deltaT);
		} else if (_estado_perseguir) {
			perseguir(deltaT);
		} else {
			atacar(deltaT);
		}
	}

	if (_animStateMorir->getEnabled() && !_animStateMorir->hasEnded()){
		addTimeAnimacion(_animStateMorir, deltaT);
	}

	if (_animStateMorir->hasEnded()){
		_animStateMorir->setEnabled(false);
		deleteEnemigo();
	}
}

void Enemigo4::andar(Ogre::Real deltaT) {
	avanceAndar(deltaT);
	if(isDetected()) {
		_estado_andar = false;
		_estado_perseguir = true;
	}
}

void Enemigo4::perseguir(Ogre::Real deltaT) {
	if (!_animStateAndar->getEnabled()){
		configurarAnimacion(_animStateAndar, true);
	}

	if (_animStateAndar->getEnabled() && !_animStateAndar->hasEnded()){
		addTimeAnimacion(_animStateAndar, deltaT*2);
	}


	_EnemigoRigidBody->activate(true);
	btVector3 vectorRobot = calcularVectorRobot();
	vectorRobot = btVector3(vectorRobot.x(), 0.0, vectorRobot.z());
	calcularLinearVelocity();

	double angulo = calcularAngulo(_linearVelocity, vectorRobot);


	if (angulo > ANGULO_CON_ROBOT){
		btVector3 perpendicular = btVector3(vectorRobot.z(), vectorRobot.y(), -vectorRobot.x());
		double anguloConPerpendicular = calcularAngulo(_linearVelocity, perpendicular);
		if(anguloConPerpendicular >= 90){
			_giroIzquierda = true;
		} else {
			_giroIzquierda = false;
		}
		girar();
	} else {
		pararGiro();
		_EnemigoRigidBody->setLinearVelocity(_linearVelocity*2);
	}


	if (isNear()){
		if(!_robot->isDead())
			_estado_atacar = true;
		_estado_perseguir = false;
	} else if (!isDetected()){
		_estado_andar = true;
		_estado_perseguir = false;
	}
}

void Enemigo4::atacar(Ogre::Real deltaT) {
	if (!isNear()) {
		_estado_atacar = false;
		_estado_perseguir = true;
	}

	if (!_animStateAtacar->hasEnded() && _animStateAtacar->getEnabled()){
		addTimeAnimacion(_animStateAtacar, deltaT);
	} else if (_animStateAtacar->hasEnded()){
		_animStateAtacar->setEnabled(false);
		quitarVidaRobot();
		reproducirGolpe();
	}

	if (!_animStateAtacar->getEnabled())
		configurarAnimacion(_animStateAtacar, false);

	pararAvance();
	pararGiro();
}

bool Enemigo4::isDetected() {
	bool isDetected = false;

	btVector3 vector = calcularVectorRobot();

	double distancia = calcularModulo(vector);

	if (8 < distancia && distancia < 50){
		isDetected = true;
	}

	return isDetected;
}

bool Enemigo4::isNear() {
	bool isNear = false;

	btVector3 vector = calcularVectorRobot();

	double distancia = calcularModulo(vector);

	if (distancia < 8) {
		isNear = true;
	}

	return isNear;
}

void Enemigo4::decrementarContador(Ogre::Real deltaT) {
	Ogre::Real deltaTime = _m_precisionTimer->getMilliseconds();
	_m_precisionTimer->reset();
	_contador -= deltaTime;
}

void Enemigo4::eliminarExplosion() {
	StringStream nombre;
	nombre << _nodeEnemigo->getName() << "particle";
	_sceneMgr->destroyParticleSystem(nombre.str());
	nombre.str("");
	nombre << _nodeEnemigo->getName() << "billboard";
	_sceneMgr->destroyBillboardSet(nombre.str());
	nombre.str("");
}


void Enemigo4::crearBillboard(){
	std::stringstream saux;
	std::string s = "billboard";
	saux << _nodeEnemigo->getName() << s;

	Ogre::BillboardSet* nameBillboardSet = _sceneMgr->createBillboardSet(saux.str(),1);
	nameBillboardSet->setBillboardType(Ogre::BBT_POINT);
	nameBillboardSet->setMaterialName("PuntosName");
	nameBillboardSet->setDefaultDimensions(3,1.5);
	nameBillboardSet->createBillboard(Ogre::Vector3(0,0,0));

	Ogre::SceneNode* puntosNameNode = _sceneMgr->getRootSceneNode()->createChildSceneNode(saux.str());
	puntosNameNode->attachObject(nameBillboardSet);
	puntosNameNode->translate(_nodeEnemigo->getPosition().x, _nodeEnemigo->getPosition().y +3,_nodeEnemigo->getPosition().z);
}

void Enemigo4::morir() {
	_isDead = true;
	_world->removeRigidBody(_EnemigoRigidBody);

	addPuntos(_puntos);

	pararAvance();
	pararGiro();

	_animStateAndar->setEnabled(false);
	_animStateAtacar->setEnabled(false);

	configurarAnimacion(_animStateMorir, false);
}

void Enemigo4::restarVida(int danio) {
	_vida -= danio;

	if (_vida <= 0 && !isDead()){
		morir();
	}
}
