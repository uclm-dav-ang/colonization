#include "ElegirNivelState.h"
#include "PlayState.h"
#include "IntroState.h"

template<> ElegirNivelState* Ogre::Singleton<ElegirNivelState>::msSingleton = 0;

void
ElegirNivelState::enter ()
{
  _animacionPlanetas.clear();
  _planetas.clear();

  _root = Ogre::Root::getSingletonPtr();

  _sceneMgr = _root->getSceneManager("SceneManager");
  _camera = _sceneMgr->getCamera("IntroCamera");
  _camera->setPosition(Ogre::Vector3(0.01, 20, 0));
  _camera->lookAt(Ogre::Vector3(0, 0, 0));
  _camera->setNearClipDistance(1);
  _camera->setFarClipDistance(100);
  _camera->setFOVy(Ogre::Degree(50));
  _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);

  crearMenu();
  inicializarAnimaciones();

  int i;
  for(i = 0; i < 3; i++){
	  _nivelesConseguidos.push_back(false);
  }

  _menuSeleccion = 0;

  _exitGame = false;
}

void
ElegirNivelState::exit()
{
  _sceneMgr->clearScene();
  _root->getAutoCreatedWindow()->removeAllViewports();
  _sceneMgr->destroyAllAnimationStates();
}

void
ElegirNivelState::pause ()
{
}

void
ElegirNivelState::resume ()
{
}

bool
ElegirNivelState::frameStarted
(const Ogre::FrameEvent& evt)
{
  Ogre::Real deltaT = evt.timeSinceLastFrame;

  int i;
  for(i = 0; i < NUMERO_NIVELES; i++){
	  if (_animacionPlanetas[i]->getEnabled() && !_animacionPlanetas[i]->hasEnded()){
		  _animacionPlanetas[i]->addTime(deltaT/6);
	  }
   }


  return true;
}

bool
ElegirNivelState::frameEnded
(const Ogre::FrameEvent& evt)
{
  if (_exitGame)
    return false;

  return true;
}

void
ElegirNivelState::keyPressed
(const OIS::KeyEvent &e)
{
  //transicion al PlayState
  if (e.key == 28) {
	  PlayState::getSingletonPtr()->setNivel(_menuSeleccion+1);
	  changeState(PlayState::getSingletonPtr());
  }
  if (e.key == OIS::KC_RIGHT && _menuSeleccion < NUMERO_NIVELES-1 && _nivelesConseguidos[_menuSeleccion]) {
    _menuSeleccion++;
    cambiarTexturaMenu();
  }

  if (e.key == OIS::KC_LEFT && _menuSeleccion > 0) {
     _menuSeleccion--;
     cambiarTexturaMenu();
   }
  if (e.key == OIS::KC_ESCAPE) {
     changeState(IntroState::getSingletonPtr());
   }

}

void
ElegirNivelState::keyReleased
(const OIS::KeyEvent &e )
{
}

void
ElegirNivelState::mouseMoved
(const OIS::MouseEvent &e)
{
}

void
ElegirNivelState::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
}

void
ElegirNivelState::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
}

void ElegirNivelState::crearMenu(){
	Ogre::SceneNode *nodePrincipal = _sceneMgr->createSceneNode("Principal");
	_sceneMgr->getRootSceneNode()->addChild(nodePrincipal);

	stringstream saux, saux2;
	string s = "Planeta";
	string s2 = "MaterialPlaneta";

	int i, positionZ;
	positionZ = 10;
	for (i = 0; i < NUMERO_NIVELES; i++){
		saux << s << (i+1);
		saux2 << s2 << (i+1);
		SceneNode *nodePlaneta = _sceneMgr->createSceneNode(saux.str());
		Entity* ent_planeta = _sceneMgr->createEntity(saux.str(), "Planeta.mesh");
		ent_planeta->setMaterialName(saux2.str());
		nodePlaneta->attachObject(ent_planeta);
		nodePrincipal->addChild(nodePlaneta);
		nodePlaneta->setPosition(0, -5, positionZ);
		positionZ -= 10;
		nodePlaneta->roll(Degree(90));
		_planetas.push_back(nodePlaneta);

		saux.str("");
		saux2.str("");
	}

	SceneNode *nodeNivel1 = _sceneMgr->createSceneNode("Nivel1");
	Entity* ent_Nivel1 = _sceneMgr->createEntity("Nivel1", "Plane.mesh");
	ent_Nivel1->setMaterialName("MaterialMenuB7");
	nodeNivel1->attachObject(ent_Nivel1);
	nodePrincipal->addChild(nodeNivel1);
	nodeNivel1->setPosition(7, -10, 12);
	nodeNivel1->yaw(Degree(90));

	SceneNode *nodeNivel2 = _sceneMgr->createSceneNode("Nivel2");
	Entity* ent_Nivel2 = _sceneMgr->createEntity("Nivel2", "Plane.mesh");
	ent_Nivel2->setMaterialName("MaterialMenuN8");
	nodeNivel2->attachObject(ent_Nivel2);
	nodePrincipal->addChild(nodeNivel2);
	nodeNivel2->setPosition(7, -10, 0);
	nodeNivel2->yaw(Degree(90));

	SceneNode *nodeNivel3 = _sceneMgr->createSceneNode("Nivel3");
	Entity* ent_Nivel3 = _sceneMgr->createEntity("Nivel3", "Plane.mesh");
	ent_Nivel3->setMaterialName("MaterialMenuN9");
	nodeNivel3->attachObject(ent_Nivel3);
	nodePrincipal->addChild(nodeNivel3);
	nodeNivel3->setPosition(7, -10, -12);
	nodeNivel3->yaw(Degree(90));




}

void ElegirNivelState::inicializarAnimaciones(){
	int i;
	for (i = 0; i < NUMERO_NIVELES; i++){
		Ogre::AnimationState *animation;
		animation = _sceneMgr->getEntity(_planetas[i]->getName())->getAnimationState("Rotar");
		animation->setTimePosition(0.0);
		animation->setEnabled(true);
		animation->setLoop(true);
		_animacionPlanetas.push_back(animation);
	}
}

void ElegirNivelState::cambiarTexturaMenu(){
	switch (_menuSeleccion){
	case 0:
		_sceneMgr->getEntity("Nivel1")->setMaterialName("MaterialMenuB7");
		_sceneMgr->getEntity("Nivel2")->setMaterialName("MaterialMenuN8");
		break;
	case 1:
		_sceneMgr->getEntity("Nivel2")->setMaterialName("MaterialMenuB8");
		_sceneMgr->getEntity("Nivel1")->setMaterialName("MaterialMenuN7");
		_sceneMgr->getEntity("Nivel3")->setMaterialName("MaterialMenuN9");
		break;
	case 2:
		_sceneMgr->getEntity("Nivel3")->setMaterialName("MaterialMenuB9");
		_sceneMgr->getEntity("Nivel2")->setMaterialName("MaterialMenuN8");
		break;
	}
}

void ElegirNivelState::setNivelConseguido(int numeroNivel){
	_nivelesConseguidos[numeroNivel-1] = true;
}

void ElegirNivelState::setNivelesConseguidos(std::vector<bool> nivelesConseguidos){
	_nivelesConseguidos = nivelesConseguidos;
}

ElegirNivelState*
ElegirNivelState::getSingletonPtr ()
{
return msSingleton;
}

ElegirNivelState&
ElegirNivelState::getSingleton ()
{
  assert(msSingleton);
  return *msSingleton;
}
