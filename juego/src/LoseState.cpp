#include "LoseState.h"
#include "IntroState.h"
#include "PlayState.h"
#include "ElegirNivelState.h"


template<> LoseState* Ogre::Singleton<LoseState>::msSingleton = 0;

void LoseState::enter() {
	_root = Ogre::Root::getSingletonPtr();

	// Se recupera el gestor de escena y la cámara.
	_sceneMgr = _root->getSceneManager("SceneManager");
	_camera = _sceneMgr->getCamera("IntroCamera");
	_camera->setPosition(Ogre::Vector3(0.01, 20, 0));
	_camera->lookAt(Ogre::Vector3(0, 0, 0));
	_viewport = _root->getAutoCreatedWindow()->addViewport(_camera);


	_overlayManager = Ogre::OverlayManager::getSingletonPtr();
	Ogre::Overlay *overlay = _overlayManager->getByName("Perder");
	overlay->show();

	_exitGame = false;
}

void LoseState::exit() {
	_root->getAutoCreatedWindow()->removeAllViewports();
	_sceneMgr->destroyAllCameras();
	_sceneMgr->clearScene();
	_root->destroySceneManager(_sceneMgr);

	Ogre::OverlayManager::getSingletonPtr()->getByName("Perder")->hide();
}

void LoseState::pause() {
}

void LoseState::resume() {
}

bool LoseState::frameStarted(const Ogre::FrameEvent& evt) {

	return true;
}

bool LoseState::frameEnded(const Ogre::FrameEvent& evt) {
	if (_exitGame)
		return false;

	return true;
}

void LoseState::keyPressed(const OIS::KeyEvent &e) {
	// Tecla p --> Estado anterior.
	if (e.key == 28) {
		int i;
		std::vector<bool> nivelesConseguidos;
		for (i = 0; i < 3; i++){
			nivelesConseguidos.push_back(false);
		}
		ElegirNivelState::getSingletonPtr()->setNivelesConseguidos(nivelesConseguidos);
		PlayState::getSingletonPtr()->setPuntos(0);
		changeState(IntroState::getSingletonPtr());
	}
}

void LoseState::keyReleased(const OIS::KeyEvent &e) {
}

void LoseState::mouseMoved(const OIS::MouseEvent &e) {
}

void LoseState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id) {
}

void LoseState::mouseReleased(const OIS::MouseEvent &e,
		OIS::MouseButtonID id) {
}

LoseState*
LoseState::getSingletonPtr() {
	return msSingleton;
}

LoseState&
LoseState::getSingleton() {
	assert(msSingleton);
	return *msSingleton;
}
