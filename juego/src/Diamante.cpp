#include <Diamante.h>
#include <OgreSceneNode.h>

Diamante::Diamante(SceneNode* nodeDiamante) {
	_nodeDiamante = nodeDiamante;

	_animStateRotarDiamante = Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager")->getEntity(_nodeDiamante->getName())->getAnimationState("RotarDiamante");
	configurarAnimacion(_animStateRotarDiamante, true);
}

Diamante::Diamante(const Diamante &obj){
	_nodeDiamante = obj.getNode();
}

Diamante::~Diamante() {
	delete _nodeDiamante;
}

Diamante& Diamante::operator= (const Diamante &obj){
	delete _nodeDiamante;
	_nodeDiamante = obj._nodeDiamante;
	return *this;
}

SceneNode* Diamante::getNode()const {
	return _nodeDiamante;
}

void Diamante::ocultar() {
	_nodeDiamante->setVisible(false);
}

void Diamante::animar(Ogre::Real deltaT){
	addTimeAnimacion(_animStateRotarDiamante, deltaT);
}

void Diamante::configurarAnimacion(Ogre::AnimationState *animState, bool loop) {
	animState->setTimePosition(0.0);
	animState->setEnabled(true);
	animState->setLoop(loop);
}

void Diamante::addTimeAnimacion(Ogre::AnimationState *animState, Ogre::Real deltaT) {
	animState->addTime(deltaT);
}

void Diamante::pararAnimacion(Ogre::AnimationState *animState) {
	animState->setEnabled(false);
}
