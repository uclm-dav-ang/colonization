#include <Robot.h>
#include <OgreSceneNode.h>
#include <cmath>
#include "Arma1.h"
#include "Arma2.h"
#include "Arma3.h"
#include "PlayState.h"

Robot::Robot(SceneNode* nodeRobot, SceneNode* nodeRobotAvance, btCollisionShape* robotShape, btRigidBody* robotRigidBody, int nivel) {
	_gameManager = GameManager::getSingletonPtr();

	_nodeRobot = nodeRobot;
	_nodeRobotAvance = nodeRobotAvance;
	_robotShape = robotShape;
	_robotRigidBody = robotRigidBody;

	_avanzar = false;
	_girarIzquierda = false;
	_girarDerecha = false;
	_estaDisparando = false;
	_saltar = false;
	_estaEnAire = false;
	_mVerticalVelocity = 0;

	_enemigoObjetivo = NULL;
	_onGround = true;

	_mName = "robot";
	_mSightNode = _nodeRobot->createChildSceneNode (_mName + "_sight", Vector3 (0, 5, -15));
	_mCameraNode = _nodeRobot->createChildSceneNode (_mName + "_camera", Vector3 (0, 7, -14));

	_vida = 100;

	_root = Ogre::Root::getSingletonPtr();
	_sceneMgr = _root->getSceneManager("SceneManager");

	_animStateDesplazarse = _sceneMgr->getEntity("Robot")->getAnimationState("Desplazarse");
	_animStateDisparo1 = _sceneMgr->getEntity("Robot")->getAnimationState("Disparar1");
	_animStateDisparo1Volver = _sceneMgr->getEntity("Robot")->getAnimationState("Disparo1Volver");
	_animStateMorir = _sceneMgr->getEntity("Robot")->getAnimationState("Morir");
	_contadorArmas = 0;

	_isFalling = false;
	_verticalCurrentPosition = _verticalLastPosition = _nodeRobot->getPosition().y;

	int i;
	for (i = 0; i < 3; i++){
		_armasConseguidas.push_back(false);
		_nivelesConseguidos.push_back(false);
	}

	_armaElegida = 0;
	_nivel = nivel;
	_puntos = 0;

	_hasPerdido = false;
}

Robot::Robot(const Robot &obj){
	_nodeRobot = obj.getNode();
	_nodeRobotAvance = obj.getNodeAvance();
	_mSightNode = obj.getSightNode();
	_mCameraNode = obj.getCameraNode();
	_robotShape = obj.getShape();
	_robotRigidBody = obj.getRigidBody();
}

Robot::~Robot() {
	delete _nodeRobot;
	delete _nodeRobotAvance;
	delete _mSightNode;
	delete _mCameraNode;
	delete _robotShape;
	delete _robotRigidBody;
}

Robot& Robot::operator= (const Robot &obj){
	delete _nodeRobot;
	delete _nodeRobotAvance;
	delete _mSightNode;
	delete _mCameraNode;
	delete _robotShape;
	delete _robotRigidBody;
	_nodeRobot = obj._nodeRobot;
	_nodeRobotAvance = obj._nodeRobotAvance;
	_mSightNode = obj._mSightNode;
	_mCameraNode = obj._mCameraNode;
	_robotShape = obj._robotShape;
	_robotRigidBody = obj._robotRigidBody;
	return *this;
}

SceneNode* Robot::getNode()const {
	return _nodeRobot;
}

SceneNode* Robot::getNodeAvance()const {
	return _nodeRobotAvance;
}

SceneNode* Robot::getSightNode()const {
	return _mSightNode;
}

SceneNode* Robot::getCameraNode()const {
	return _mCameraNode;
}

Vector3 Robot::getWorldPosition(){
	return _nodeRobot->_getDerivedPosition ();
}

btCollisionShape* Robot::getShape()const {
	return _robotShape;
}

btRigidBody* Robot::getRigidBody()const {
	return _robotRigidBody;
}

void Robot::calcularLinearVelocity() {
	double x = 5 * (_nodeRobotAvance->_getDerivedPosition().x-_nodeRobot->getPosition().x);
	double z = 5 * (_nodeRobotAvance->_getDerivedPosition().z-_nodeRobot->getPosition().z);
	_vectorDireccion = btVector3(x, 0, z);

	if(!_avanzar){
		x = 0;
		z = 0;
	}

	double verticalVelocity;
	verticalVelocity = calcularVerticalVelocity();

	_linearVelocity = btVector3(x,verticalVelocity,z);
}

btVector3 Robot::getLinearVelocity() {
	return _linearVelocity;
}

btVector3 Robot::getVectorDireccion() {
	return _vectorDireccion;
}

void Robot::actuar(Ogre::Real deltaT){
	if((_avanzar) || (_saltar) || (_estaEnAire) || (_estaDisparando)){
		calcularLinearVelocity();
		_robotRigidBody->activate(true);
		_robotRigidBody->setLinearVelocity(_linearVelocity*2);
	}

	if(isDead()){
		if (_animStateMorir->getEnabled() && !_animStateMorir->hasEnded()){
			addTimeAnimacion(_animStateMorir, deltaT);
		}

		if (_animStateMorir->hasEnded()){
			_animStateMorir->setEnabled(false);
			_hasPerdido = true;
		}
	}

	if (hasCaido())
		_hasPerdido = true;

	actualizarDisparos(deltaT);

	if (_avanzar && !_estaDisparando)
		avanzar(deltaT);

	if (_girarIzquierda && !_estaDisparando)
		girarIzquierda();

	if (_girarDerecha && !_estaDisparando)
		girarDerecha();

	if (_estaDisparando){
		pararAvance();
		pararGiro();
		checkAnimacionDisparo(deltaT);
	}

}

void Robot::avanzar(Ogre::Real deltaT) {
	if (!_animStateDesplazarse->getEnabled()){
		configurarAnimacion(_animStateDesplazarse, true);
	}

	if (_animStateDesplazarse->getEnabled() && !_animStateDesplazarse->hasEnded()){
		addTimeAnimacion(_animStateDesplazarse, deltaT);
	}
}

void Robot::girarIzquierda() {
	_robotRigidBody->activate(true);
	_robotRigidBody->setAngularVelocity(btVector3(0.0, 2.0, 0.0));
}

void Robot::girarDerecha() {
	_robotRigidBody->activate(true);
	_robotRigidBody->setAngularVelocity(btVector3(0.0, -2.0, 0.0));
}

void Robot::pararAvance() {
	_robotRigidBody->setLinearVelocity(btVector3(0.0, 0.0, 0.0));
	pararAnimacion(_animStateDesplazarse);
}

void Robot::pararGiro() {
	_robotRigidBody->activate(true);
	_robotRigidBody->setAngularVelocity(btVector3(0.0, 0.0, 0.0));
	_robotRigidBody->setLinearVelocity(btVector3(0.0, 0.0, 0.0));
}

void Robot::addEnemigo(Enemigo* enemigo){
	_enemigos.push_back(enemigo);
}

std::vector<Enemigo*> Robot::getEnemigos() {
	return _enemigos;
}

void Robot::setEnemigos(std::vector<Enemigo*> enemigos){
	_enemigos = enemigos;
}

void Robot::addArma(Arma* arma){
	_armas.push_back(arma);
}

std::vector<Arma*> Robot::getArmas() {
	return _armas;
}

std::vector<bool> Robot::getArmasConseguidas() {
	return _armasConseguidas;
}

void Robot::setArmas(std::vector<Arma*> armas){
	_armas = armas;
}

void Robot::addDiamante(Diamante* diamante){
	_diamantes.push_back(diamante);
}

std::vector<Diamante*> Robot::getDiamantes() {
	return _diamantes;
}

void Robot::setDiamantes(std::vector<Diamante*> diamantes){
	_diamantes = diamantes;
}

void Robot::addPilaVida(PilaVida* pilavida){
	_pilasVida.push_back(pilavida);
}

std::vector<PilaVida*> Robot::getPilaVida() {
	return _pilasVida;
}

void Robot::setPilaVida(std::vector<PilaVida*> pilasVida){
	_pilasVida = pilasVida;
}

void Robot::addObjetoArma(ObjetoArma* objetoArma){
	_objetoArmas.push_back(objetoArma);
}

std::vector<ObjetoArma*> Robot::getObjetoArma(){
	return _objetoArmas;
}

void Robot::setObjetoArma(std::vector<ObjetoArma*> objetoArma){
	_objetoArmas = objetoArma;
}

void Robot::calcularEnemigosCercanos() {
	double anguloLimite = 30.0;
	double distanciaLimite = 40.0;

	_enemigosCercanos.clear();

	int i;
	for (i = 0; i < (int)_enemigos.size(); i++){
		btVector3 vectorEnemigo = calcularVectorEnemigo(_enemigos[i]);
		double distancia = calcularModulo(vectorEnemigo);
		double angulo = calcularAngulo(_vectorDireccion, vectorEnemigo);
		if(distancia <= distanciaLimite && angulo <= anguloLimite && !_enemigos[i]->isDead()){
			_enemigosCercanos.push_back(_enemigos[i]);
		}
	}
}

void Robot::calcularEnemigoObjetivo(){
	int i;
	_enemigoObjetivo = NULL;
	calcularLinearVelocity();
	calcularEnemigosCercanos();
	double distanciaMenor = 1000.0;
	for (i = 0; i < (int)_enemigosCercanos.size(); i++){
		btVector3 vectorEnemigo = calcularVectorEnemigo(_enemigosCercanos[i]);
		double distancia = calcularModulo(vectorEnemigo);
		if (distancia < distanciaMenor){
			distanciaMenor = distancia;
			_enemigoObjetivo = _enemigosCercanos[i];
		}
	}
}

int Robot::getVida(){
	return _vida;
}

void Robot::setVida(int vida) {
	_vida = vida;

	if (isDead()){
		_vida = 0;
		morir();
	}
}

void Robot::configurarAnimacion(Ogre::AnimationState *animState, bool loop) {
	animState->setTimePosition(0.0);
	animState->setEnabled(true);
	animState->setLoop(loop);
}

void Robot::addTimeAnimacion(Ogre::AnimationState *animState, Ogre::Real deltaT) {
	animState->addTime(deltaT);
}

void Robot::pararAnimacion(Ogre::AnimationState *animState) {
	animState->setEnabled(false);
}

btVector3 Robot::calcularVectorEnemigo(Enemigo *enemigo) {
	double x = (enemigo->getNode()->_getDerivedPosition().x-_nodeRobot->_getDerivedPosition().x);
	double z = (enemigo->getNode()->_getDerivedPosition().z-_nodeRobot->_getDerivedPosition().z);

	btVector3 vector(x,0,z);

	return vector;

}

double Robot::calcularModulo(btVector3 vector) {
	double modulo = sqrt(pow(vector.x(),2)+pow(vector.y(),2)+pow(vector.z(),2));

	return modulo;

}

double Robot::calcularAngulo(btVector3 vector1, btVector3 vector2) {
	double coseno = (vector1.x()*vector2.x() + vector1.z()*vector2.z())/(calcularModulo(vector1)*calcularModulo(vector2));
	double angulo = acos(coseno)*(180/3.1415);

	return angulo;
}

void Robot::disparar() {
	Ogre::stringstream nombre;
	if (_armaElegida != 0){
		nombre << "arma" << _contadorArmas;
		_contadorArmas++;
		calcularEnemigoObjetivo();
	}


	Arma *arma;
	switch (_armaElegida){
	case 1:
		arma = new Arma1(_enemigoObjetivo, nombre.str(), this);
		break;
	case 2:
		arma = new Arma2(_enemigoObjetivo, nombre.str(), this);
		break;
	case 3:
		arma = new Arma3(_enemigoObjetivo, nombre.str(), this);
		break;
	}

	if(_armaElegida != 0){
		addArma(arma);
		nombre.str("");
	}

	_gameManager->getSoundDisparo()->play();
}

void Robot::actualizarDisparos(Ogre::Real deltaT) {
	int i;
	for (i = 0; i < (int)_armas.size(); i++) {
		_armas[i]->actualizar(deltaT);
	}
}

void Robot::setOnGround(bool onGround) {
	_onGround = onGround;
}

bool Robot::isOnGround() {
	return _onGround;
}

bool Robot::estaEnAire() {
	return _estaEnAire;
}

bool Robot::getSaltar() {
	return _saltar;
}

bool Robot::conseguirObjeto(Ogre::Real deltaT){
	bool conseguir = false;

	for(int i =0; i < (int)_diamantes.size(); i++){
		_diamantes[i]->animar(deltaT);
		Vector3 objetoPosition(_diamantes[i]->getNode()->getPosition());
		Vector3 robotPosition(_nodeRobot->getPosition());
		if ((robotPosition.x+1.72 > objetoPosition.x-0.3 && robotPosition.x-1.72 < objetoPosition.x+0.3) && (robotPosition.y+3.6 > objetoPosition.y-1.25 && robotPosition.y-3.6 < objetoPosition.y+1.25) && (robotPosition.z+1.72 > objetoPosition.z-0.3 && robotPosition.z-1.72 < objetoPosition.z+0.3)){
			conseguir = true;
			_diamantes[i]->ocultar();
			_diamantes.erase(_diamantes.begin() + i);
		}
	}

	return conseguir;
}

void Robot::conseguirPilaVida(Ogre::Real deltaT){
	for(int i =0; i < (int)_pilasVida.size(); i++){
		_pilasVida[i]->animar(deltaT);
		Vector3 pilaPosition(_pilasVida[i]->getNode()->getPosition());
		Vector3 robotPosition(_nodeRobot->getPosition());
		if ((robotPosition.x+1.72 > pilaPosition.x-0.5 && robotPosition.x-1.72 < pilaPosition.x+0.5) && (robotPosition.y+3.6 > pilaPosition.y-1 && robotPosition.y-3.6 < pilaPosition.y+1) && (robotPosition.z+1.72 > pilaPosition.z-0.58 && robotPosition.z-1.72 < pilaPosition.z+0.58)){
			setVida(_pilasVida[i]->getVida()+getVida());
			ajustarVida();
			_pilasVida[i]->ocultar();
			_pilasVida.erase(_pilasVida.begin() + i);
		}
	}
}

void Robot::conseguirArma(Ogre::Real deltaT){
	for(int i = 0; i < (int)_objetoArmas.size(); i++){
		_objetoArmas[i]->animar(deltaT);
		Vector3 objetoArmaPosition(_objetoArmas[i]->getNode()->getPosition());
		Vector3 robotPosition(_nodeRobot->getPosition());
		if ((robotPosition.x+1.72 > objetoArmaPosition.x-1.3 && robotPosition.x-1.72 < objetoArmaPosition.x+1.3) && (robotPosition.y+1.75 > objetoArmaPosition.y-1 && robotPosition.y-3.6 < objetoArmaPosition.y+1.75) && (robotPosition.z+1.72 > objetoArmaPosition.z-0.5 && robotPosition.z-1.72 < objetoArmaPosition.z+0.5)){
			_objetoArmas[i]->ocultar();
			_objetoArmas.erase(_objetoArmas.begin() + i);

			setArmaConseguida(_nivel);

			mostrarOverlayArma();
		}
	}
}

void Robot::mostrarOverlayArma(){
	std::stringstream saux;
	std::string s = "Arma";
	saux << s << _nivel;
	_overlayManager = Ogre::OverlayManager::getSingletonPtr();
	Ogre::Overlay *overlayArma = _overlayManager->getByName(saux.str());
	overlayArma->show();
}

void Robot::setAvanzar(bool activado){
	_avanzar = activado;
}

void Robot::setGirarIzquierda(bool activado){
	_girarIzquierda = activado;
}

void Robot::setEstaDisparando(bool activado){
	if(!_saltar && !_estaEnAire && !isDead()){
		_estaDisparando = activado;
	}
}

void Robot::setGirarDerecha(bool activado){
	_girarDerecha = activado;
}

void Robot::setSaltar(bool activado){
	if(isOnGround()){
		_saltar = activado;
	}
}

double Robot::calcularVerticalVelocity(){
	double verticalVelocity = 0;
	if (isOnGround() && isFalling()){
		_estaEnAire = false;
	}else{
		_estaEnAire = true;
	}
	if (_saltar){
		_saltar = false;
		_estaEnAire = true;
		_mVerticalVelocity = 7;
	}
	if(_estaEnAire){
		verticalVelocity = _mVerticalVelocity;
		_mVerticalVelocity -= 0.2;
	}
	if(isOnGround() && !_estaEnAire){
		verticalVelocity = 0;
	}

	return verticalVelocity;
}

void Robot::checkAnimacionDisparo (Ogre::Real deltaT) {
	Ogre::Real avanceAnimacion;

	switch(_armaElegida){
	case 1:
		avanceAnimacion = deltaT*2;
		break;
	case 2:
		avanceAnimacion = deltaT;
		break;
	case 3:
		avanceAnimacion = deltaT/2;
		break;
	}

	if (!_animStateDisparo1->getEnabled()){
		configurarAnimacion(_animStateDisparo1, false);
	}
	if (_animStateDisparo1->getEnabled() && !_animStateDisparo1->hasEnded()){
		addTimeAnimacion(_animStateDisparo1, avanceAnimacion);
	}

	if (_animStateDisparo1->hasEnded() && !_animStateDisparo1Volver->getEnabled()){
		disparar();

		if (!_animStateDisparo1Volver->getEnabled()){
			configurarAnimacion(_animStateDisparo1Volver, false);
		}
	}

	if (_animStateDisparo1Volver->getEnabled() && !_animStateDisparo1Volver->hasEnded()){
		addTimeAnimacion(_animStateDisparo1Volver, avanceAnimacion);
	}
	if (_animStateDisparo1Volver->hasEnded()){
		_animStateDisparo1Volver->setEnabled(false);
		_animStateDisparo1->setEnabled(false);
		_animStateDisparo1->setTimePosition(0);
		_animStateDisparo1Volver->setTimePosition(0);
		_estaDisparando = false;
	}
}

void Robot::ajustarVida(){
	if (getVida() > 100){
		setVida(100);
	}
}

bool Robot::isFalling() {
	_isFalling = false;
	if (_mVerticalVelocity < 0) {
		_isFalling = true;
	}

	return _isFalling;
}

void Robot::setArmaConseguida(int numeroArma){
	_armasConseguidas[numeroArma-1] = 1;
	setArmaSeleccionada(numeroArma);
}

void Robot::setArmaSeleccionada(int numeroArma){
	if (_armasConseguidas[numeroArma-1]){
		_armaElegida = numeroArma;

		switch(_armaElegida){
		case 1:
			PlayState::getSingletonPtr()->mostrarOverlayArmaConseguidaSeleccionada1();
			break;
		case 2:
			PlayState::getSingletonPtr()->mostrarOverlayArmaConseguidaSeleccionada2();
			break;
		case 3:
			PlayState::getSingletonPtr()->mostrarOverlayArmaConseguidaSeleccionada3();
			break;
		}
	}
}

void Robot::setArmasConseguidas(std::vector<bool> armasConseguidas){
	_armasConseguidas = armasConseguidas;
	setArmaSeleccionada(1);
}

int Robot::getArmaSeleccionada(){
	return _armaElegida;
}

void Robot::addPuntos(int puntos){
	_puntos += puntos;
}

int Robot::getPuntos(){
	return _puntos;
}

void Robot::setPuntos(int puntos){
	_puntos = puntos;
}

bool Robot::isDead(){
	if (_vida <= 0) {
		return true;
	}

	return false;
}

bool Robot::hasPerdido() {
	return _hasPerdido;
}

void Robot::morir(){
	_puntos = 0;

	if (!_animStateMorir->getEnabled()){
		configurarAnimacion(_animStateMorir, false);
	}

	_gameManager->getSoundMuerteRobot()->play();
}

bool Robot::hasCaido(){
	if (_nodeRobot->getPosition().y < -10)
		return true;

	return false;
}
