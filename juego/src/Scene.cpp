#include <Scene.h>

Scene::Scene ()
{
}

Scene::~Scene ()
{
}

void
Scene::addCamera
(CameraData* camera)
{
  _camerasData.push_back(camera);
}

void Scene::setCharacterData(CharacterData* characterData){
	_characterData = characterData;
}

void Scene::addEnemigo(EnemigoData* enemigoData){
	_enemigosData.push_back(enemigoData);
}

void Scene::addDiamante(DiamanteData* diamanteData){
	_diamantesData.push_back(diamanteData);
}

void Scene::addPilaVida(PilaVidaData* pilaVidaData){
	_pilaVidaData.push_back(pilaVidaData);
}

void Scene::addObjetoArma(ObjetoArmaData* objetoArmaData){
	_objetoArmaData.push_back(objetoArmaData);
}

void Scene::setEnemigoFinal(EnemigoFinalData* enemigofinal) {
	_enemigoFinal = enemigofinal;
}
