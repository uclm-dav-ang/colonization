#include "../include/InstruccionesState.h"

#include "IntroState.h"
#include "PlayState.h"

template<> InstruccionesState* Ogre::Singleton<InstruccionesState>::msSingleton = 0;

void InstruccionesState::enter() {
	_root = Ogre::Root::getSingletonPtr();

	// Se recupera el gestor de escena y la cámara.
	_sceneMgr = _root->getSceneManager("SceneManager");
	_camera = _sceneMgr->getCamera("IntroCamera");
	_viewport = _root->getAutoCreatedWindow()->addViewport(_camera);

	Ogre::SceneNode *node_menu = _sceneMgr->createSceneNode("Menu");
	_sceneMgr->getRootSceneNode()->addChild(node_menu);

	//mostramos el overlay
	_overlayManager = Ogre::OverlayManager::getSingletonPtr();
	Ogre::Overlay *overlay = _overlayManager->getByName("Instrucciones");
	overlay->show();

	_exitGame = false;
}

void InstruccionesState::exit() {
	_root->getAutoCreatedWindow()->removeAllViewports();
	_sceneMgr->destroyAllCameras();
	_sceneMgr->clearScene();
	_root->destroySceneManager(_sceneMgr);
	Ogre::OverlayManager::getSingletonPtr()->getByName("Instrucciones")->hide();
}

void InstruccionesState::pause() {
}

void InstruccionesState::resume() {
}

bool InstruccionesState::frameStarted(const Ogre::FrameEvent& evt) {
	return true;
}

bool InstruccionesState::frameEnded(const Ogre::FrameEvent& evt) {
	if (_exitGame)
		return false;

	return true;
}

void InstruccionesState::keyPressed(const OIS::KeyEvent &e) {

}

void InstruccionesState::keyReleased(const OIS::KeyEvent &e) {
	 if (e.key == OIS::KC_ESCAPE) {
		_exitGame = true;
		 changeState(IntroState::getSingletonPtr());
	  }
}

void InstruccionesState::mouseMoved(const OIS::MouseEvent &e) {
}

void InstruccionesState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id) {
}

void InstruccionesState::mouseReleased(const OIS::MouseEvent &e,
		OIS::MouseButtonID id) {
}

InstruccionesState*
InstruccionesState::getSingletonPtr() {
	return msSingleton;
}

InstruccionesState&
InstruccionesState::getSingleton() {
	assert(msSingleton);
	return *msSingleton;
}
