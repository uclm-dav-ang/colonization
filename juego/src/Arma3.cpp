#include <Arma3.h>
#include <OgreSceneNode.h>
#include <cmath>

Arma3::Arma3(Enemigo* enemigoObjetivo, String nombre, Robot *robot) : Arma(enemigoObjetivo, nombre, robot) {
	_danio = 100;

	_nodeArma = _sceneMgr->createSceneNode(nombre);
	Ogre::Entity* entArma = _sceneMgr->createEntity(nombre, "Disparo1.mesh");
	_nodeArma->attachObject(entArma);
	entArma->setMaterialName("MaterialDisparo3");
	_nodeArma->setScale(3,3,3);
	_sceneMgr->getRootSceneNode()->addChild(_nodeArma);
	SceneNode *nodo = _robot->getNode();
	Bone* bone = _sceneMgr->getEntity(nodo->getName())->getSkeleton()->getBone("ManoDerecha");
	Vector3 posicion = nodo->_getDerivedPosition() + nodo->_getDerivedOrientation() * bone->_getDerivedPosition();
	_nodeArma->setPosition(posicion);

	//particula del arma
	std::stringstream saux;
	std::string s = "particle";
	saux << nombre << s;
	Ogre::ParticleSystem* ps = _sceneMgr->createParticleSystem(saux.str(),"disparo3");
	Ogre::SceneNode* psNode = _sceneMgr->createSceneNode(saux.str());
	psNode->attachObject(ps);

	_nodeArma->addChild(psNode);
}

Arma3::Arma3(const Arma3 &obj) : Arma (obj){
	_nodeArma = obj.getNode();
	_enemigoObjetivo = obj.getEnemigoObjetivo();
	_robot = obj.getRobot();

	_danio = 100;
	_contador = 2000;
	_m_precisionTimer = new Ogre::Timer();


	_root = Ogre::Root::getSingletonPtr();
	_sceneMgr = _root->getSceneManager("SceneManager");
}

Arma3::~Arma3() {
	delete _nodeArma;
	delete _enemigoObjetivo;
}

Arma3& Arma3::operator= (const Arma3 &obj){
	delete _nodeArma;
	delete _enemigoObjetivo;
	delete _robot;
	_nodeArma = obj._nodeArma;
	_enemigoObjetivo = obj._enemigoObjetivo;

	return *this;
}
