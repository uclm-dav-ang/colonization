#include <PilaVida.h>
#include <OgreSceneNode.h>

PilaVida::PilaVida(SceneNode* nodePilaVida) {
	_nodePilaVida = nodePilaVida;
	_vida = 25;

	_animStateRotarPila = Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager")->getEntity(_nodePilaVida->getName())->getAnimationState("RotarPila");
	configurarAnimacion(_animStateRotarPila, true);
}

PilaVida::PilaVida(const PilaVida &obj){
	_nodePilaVida = obj.getNode();
	_vida = 25;
}

PilaVida::~PilaVida() {
	delete _nodePilaVida;
}

PilaVida& PilaVida::operator= (const PilaVida &obj){
	delete _nodePilaVida;
	_nodePilaVida = obj._nodePilaVida;
	return *this;
}

SceneNode* PilaVida::getNode()const {
	return _nodePilaVida;
}

void PilaVida::ocultar() {
	_nodePilaVida->setVisible(false);
}

int PilaVida::getVida(){
	return _vida;
}

void PilaVida::animar(Ogre::Real deltaT){
	addTimeAnimacion(_animStateRotarPila, deltaT);
}

void PilaVida::configurarAnimacion(Ogre::AnimationState *animState, bool loop) {
	animState->setTimePosition(0.0);
	animState->setEnabled(true);
	animState->setLoop(loop);
}

void PilaVida::addTimeAnimacion(Ogre::AnimationState *animState, Ogre::Real deltaT) {
	animState->addTime(deltaT);
}

void PilaVida::pararAnimacion(Ogre::AnimationState *animState) {
	animState->setEnabled(false);
}
