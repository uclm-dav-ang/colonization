#include <Arma.h>
#include <OgreSceneNode.h>
#include <cmath>

Arma::Arma(Enemigo* enemigoObjetivo, String nombre, Robot *robot) {
	_enemigoObjetivo = enemigoObjetivo;
	_robot = robot;
	btVector3 linearVelocity = _robot->getVectorDireccion();
	_vectorAvance = Vector3(linearVelocity.x(), linearVelocity.y(), linearVelocity.z());

	_root = Ogre::Root::getSingletonPtr();
	_sceneMgr = _root->getSceneManager("SceneManager");

	_contador = 2000;
	_m_precisionTimer = new Ogre::Timer();
}

Arma::Arma(const Arma &obj){
	_nodeArma = obj.getNode();
	_enemigoObjetivo = obj.getEnemigoObjetivo();
	_robot = obj.getRobot();

	_contador = 2000;
	_m_precisionTimer = new Ogre::Timer();


	_root = Ogre::Root::getSingletonPtr();
	_sceneMgr = _root->getSceneManager("SceneManager");
}

Arma::~Arma() {
	delete _nodeArma;
	delete _enemigoObjetivo;
}

Arma& Arma::operator= (const Arma &obj){
	delete _nodeArma;
	delete _enemigoObjetivo;
	delete _robot;
	_nodeArma = obj._nodeArma;
	_enemigoObjetivo = obj._enemigoObjetivo;

	return *this;
}

SceneNode* Arma::getNode()const {
	return _nodeArma;
}

Enemigo* Arma::getEnemigoObjetivo()const {
	return _enemigoObjetivo;
}

Robot* Arma::getRobot()const {
	return _robot;
}


void Arma::calcularVectorAvance() {
	//calculamos la direccion del impulso
	double x = 5 * (_enemigoObjetivo->getNode()->_getDerivedPosition().x-_nodeArma->_getDerivedPosition().x);
	double y = 5 * (_enemigoObjetivo->getNode()->_getDerivedPosition().y-_nodeArma->_getDerivedPosition().y);
	double z = 5 * (_enemigoObjetivo->getNode()->_getDerivedPosition().z-_nodeArma->_getDerivedPosition().z);

	_vectorAvance = Vector3(x,y,z);
}

Vector3 Arma::getVectorAvance() {
	return _vectorAvance;
}

void Arma::actualizar(Ogre::Real deltaT) {
	avanzar();

	decrementarContador(deltaT);

	if(_contador <= 0){
		deleteArma();
	}

	if(_enemigoObjetivo != NULL){
		if(hasCollision()){
			quitarVidaEnemigo();
			deleteArma();
		}
	}
}
void Arma::avanzar() {
	if(_enemigoObjetivo!= NULL){
		calcularVectorAvance();
	}
	Vector3 vectorUnitario = calcularVectorUnitario(_vectorAvance);
	_nodeArma->translate(vectorUnitario);
}


Vector3 Arma::calcularVectorUnitario(Vector3 vector) {
	Vector3 unitario(0.0, 0.0, 0.0);
	if (calcularModulo(vector) != 0)
		unitario = vector/calcularModulo(vector);

	return unitario;
}

double Arma::calcularModulo(Vector3 vector) {
	double modulo = sqrt(pow(vector.x,2)+pow(vector.z,2));

	return modulo;

}

bool Arma::hasCollision() {
	bool collision = false;
	Vector3 armaPosition(_nodeArma->getPosition());
	Vector3 enemigoPosition(_enemigoObjetivo->getNode()->getPosition());
	if ((enemigoPosition.x-0.5 < armaPosition.x && armaPosition.x < enemigoPosition.x+0.5) && (enemigoPosition.y-0.5 < armaPosition.y && armaPosition.y < enemigoPosition.y+0.5) && (enemigoPosition.z-0.5 < armaPosition.z && armaPosition.z < enemigoPosition.z+0.5)){
		collision = true;
	}

	return collision;
}

void Arma::quitarVidaEnemigo() {
	_enemigoObjetivo->restarVida(_danio);
}

void Arma::deleteArma() {
	int i;
	std::vector<Arma*> armas = _robot->getArmas();
	for (i = 0; i < (int)armas.size(); i++){
		if(this == armas[i]){
			armas.erase(armas.begin()+i);
		}
	}
	_robot->setArmas(armas);
	ocultar();
	//delete(this);
}

void Arma::ocultar() {
	_nodeArma->setVisible(false);
}

void Arma::decrementarContador(Ogre::Real deltaT){
	Ogre::Real deltaTime = _m_precisionTimer->getMilliseconds();
	_m_precisionTimer->reset();
	_contador -= deltaTime;
}
