#include <Enemigo.h>
#include <OgreSceneNode.h>
#include <cmath>

Enemigo::Enemigo(String nombre, btVector3 posicion, Robot *robot, btDiscreteDynamicsWorld* world) {
	_gameManager = GameManager::getSingletonPtr();

	_mName = nombre;
	_initialPosition = posicion;
	_world = world;
	_robot = robot;
	_isDead = false;

	_root = Ogre::Root::getSingletonPtr();
	_sceneMgr = _root->getSceneManager("SceneManager");

	int aleatorio = rand() * 100+1;
	if (aleatorio < 50){
		_giroIzquierda = true;
	} else {
		_giroIzquierda = false;
	}

	_contadorAndar = TIEMPO_ANDAR;
	_m_precisionTimerAndar = new Ogre::Timer();
}

Enemigo::Enemigo(const Enemigo &obj){
	_nodeEnemigo = obj.getNode();
	_nodeEnemigoAvance = obj.getNodeAvance();
	_EnemigoShape = obj.getShape();
	_EnemigoRigidBody = obj.getRigidBody();
	_robot = obj.getRobot();
}

Enemigo::~Enemigo() {
	delete _nodeEnemigo;
	delete _nodeEnemigoAvance;
	delete _EnemigoShape;
	delete _EnemigoRigidBody;
}

Enemigo& Enemigo::operator= (const Enemigo &obj){
	delete _nodeEnemigo;
	delete _nodeEnemigoAvance;
	delete _EnemigoShape;
	delete _EnemigoRigidBody;
	_nodeEnemigo = obj._nodeEnemigo;
	_nodeEnemigo =obj._nodeEnemigoAvance;
	_EnemigoShape = obj._EnemigoShape;
	_EnemigoRigidBody = obj._EnemigoRigidBody;
	return *this;
}

SceneNode* Enemigo::getNode()const {
	return _nodeEnemigo;
}

SceneNode* Enemigo::getNodeAvance()const {
	return _nodeEnemigoAvance;
}

btCollisionShape* Enemigo::getShape()const {
	return _EnemigoShape;
}

btRigidBody* Enemigo::getRigidBody()const {
	return _EnemigoRigidBody;
}

Robot* Enemigo::getRobot() const{
	return _robot;
}

btVector3 Enemigo::calcularVectorRobot() {
	double x = (_robot->getNode()->_getDerivedPosition().x-_nodeEnemigo->getPosition().x);
	double z = (_robot->getNode()->_getDerivedPosition().z-_nodeEnemigo->getPosition().z);

	btVector3 vector(x,0,z);

	return vector;

}

double Enemigo::calcularModulo(btVector3 vector) {
	double modulo = sqrt(pow(vector.x(),2)+pow(vector.z(),2));

	return modulo;

}

void Enemigo::calcularLinearVelocity() {
	//calculamos la direccion del impulso
	double x = 5 * (_nodeEnemigoAvance->_getDerivedPosition().x-_nodeEnemigo->getPosition().x);
	double z = 5 * (_nodeEnemigoAvance->_getDerivedPosition().z-_nodeEnemigo->getPosition().z);

	double verticalVelocity;
	if(_nodeEnemigo->_getDerivedPosition().y > (_centro + 1)) {
		verticalVelocity = -5;
	} else {
		verticalVelocity = 0;
	}

	_linearVelocity = btVector3(x,verticalVelocity,z);
}

double Enemigo::calcularAngulo(btVector3 vector1, btVector3 vector2) {
	double coseno = (vector1.x()*vector2.x() + vector1.z()*vector2.z())/(calcularModulo(vector1)*calcularModulo(vector2));
	double angulo = acos(coseno)*(180/3.1415);

	return angulo;
}

void Enemigo::girar(){
	btVector3 vector;
	if (_giroIzquierda){
		vector = btVector3(0.0, 5.0, 0.0);
	} else {
		vector = btVector3(0.0, -5.0, 0.0);
	}
	_EnemigoRigidBody->setAngularVelocity(vector);
}

void Enemigo::pararGiro(){
	_EnemigoRigidBody->setAngularVelocity(btVector3(0.0, 0.0, 0.0));
}

void Enemigo::pararAvance(){
	_EnemigoRigidBody->setLinearVelocity(btVector3(0.0, 0.0, 0.0));
}

void Enemigo::quitarVidaRobot(){
	_robot->setVida(_robot->getVida() - _poder_ataque);
}

void Enemigo::avanceAndar(Ogre::Real deltaT) {
	Ogre::Real deltaTime = _m_precisionTimerAndar->getMilliseconds();
	_m_precisionTimerAndar->reset();
	_contadorAndar -= deltaTime;

	if(_contadorAndar > 0){
		calcularLinearVelocity();
		_EnemigoRigidBody->setLinearVelocity(_linearVelocity);

		if (!_animStateAndar->getEnabled()){
			configurarAnimacion(_animStateAndar, true);
		}

		if (_animStateAndar->getEnabled() && !_animStateAndar->hasEnded()){
			addTimeAnimacion(_animStateAndar, deltaT);
		}

	} else if (_contadorAndar > -TIEMPO_GIRAR) {
		pararAvance();
		girar();
	} else {
		pararAvance();
		pararGiro();
		_contadorAndar = TIEMPO_ANDAR;

		int aleatorio = rand() * 100+1;
		if (aleatorio < 50){
			_giroIzquierda = true;
		} else {
			_giroIzquierda = false;
		}
	}
}

void Enemigo::configurarAnimacion(Ogre::AnimationState *animState, bool loop) {
	animState->setTimePosition(0.0);
	animState->setEnabled(true);
	animState->setLoop(loop);
}

void Enemigo::addTimeAnimacion(Ogre::AnimationState *animState, Ogre::Real deltaT) {
	animState->addTime(deltaT);
}

void Enemigo::pararAnimacion(Ogre::AnimationState *animState) {
	animState->setEnabled(false);
}

bool Enemigo::isDead(){
	return _isDead;
}

void Enemigo::deleteEnemigo(){
	int i;
	std::vector<Enemigo*> enemigos = _robot->getEnemigos();
	for (i = 0; i < (int)enemigos.size(); i++){
		if(this == enemigos[i]){
			enemigos.erase(enemigos.begin()+i);
		}
	}
	ocultar();
	_robot->setEnemigos(enemigos);
}

void Enemigo::ocultar() {
	_nodeEnemigo->setVisible(false);
}

void Enemigo::addPuntos(int puntos){
	_robot->addPuntos(puntos);
}

void Enemigo::reproducirGolpe(){
	_gameManager->getSoundGolpe()->play();
}
