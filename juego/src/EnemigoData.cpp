#include <EnemigoData.h>
#include <OgreSceneNode.h>
#include <cmath>

EnemigoData::EnemigoData
(Ogre::Vector3 position, int type)
{
	_position = position;
	_type = type;
}

EnemigoData::~EnemigoData ()
{
}

Vector3 EnemigoData::getPosition()const {
	return _position;
}

void EnemigoData::setPosition(Vector3 position){
	_position = position;
}


int EnemigoData::getType()const {
	return _type;
}

void EnemigoData::setType(int type){
	_type = type;
}

