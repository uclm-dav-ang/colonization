#include <EnemigoFinalData.h>
#include <OgreSceneNode.h>
#include <cmath>

EnemigoFinalData::EnemigoFinalData
(Ogre::Vector3 position, int type)
{
	_position = position;
	_type = type;
}

EnemigoFinalData::~EnemigoFinalData ()
{
}

Vector3 EnemigoFinalData::getPosition()const {
	return _position;
}

void EnemigoFinalData::setPosition(Vector3 position){
	_position = position;
}


int EnemigoFinalData::getType()const {
	return _type;
}

void EnemigoFinalData::setType(int type){
	_type = type;
}

