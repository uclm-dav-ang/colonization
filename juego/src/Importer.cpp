/* **********************************************************
** Importador NoEscape 1.0
** Curso de Experto en Desarrollo de Videojuegos 
** Escuela Superior de Informatica - Univ. Castilla-La Mancha
** Carlos Gonzalez Morcillo - David Vallejo Fernandez
************************************************************/

#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/util/PlatformUtils.hpp>
#include <OgreVector3.h>
#include <OgreVector4.h>
#include <iostream>
#include <Importer.h>

using namespace std;
using namespace xercesc;

template<> Importer* Ogre::Singleton<Importer>::msSingleton = 0;

Importer* Importer::getSingletonPtr(void)
{
    return msSingleton;
}
Importer& Importer::getSingleton(void)
{  
    assert( msSingleton );  return ( *msSingleton );  
}

void
Importer::parseScene
(const char* path, Scene* scene)
{
  // Inicialización.
  try {
    XMLPlatformUtils::Initialize();
  }
  catch (const XMLException& toCatch) {
    char* message = XMLString::transcode(toCatch.getMessage());
    cout << "Error durante la inicialización! :\n"
	 << message << "\n";
    XMLString::release(&message);
    return;
  }

  XercesDOMParser* parser = new XercesDOMParser();
  parser->setValidationScheme(XercesDOMParser::Val_Always);

  // 'Parseando' el fichero xml...
  try {
    parser->parse(path);
  }
  catch (const XMLException& toCatch) {
    char* message = XMLString::transcode(toCatch.getMessage());
    cout << "Excepción capturada: \n"
	 << message << "\n";
    XMLString::release(&message);
  }
  catch (const DOMException& toCatch) {
    char* message = XMLString::transcode(toCatch.msg);
    cout << "Excepción capturada: \n"
	 << message << "\n";
    XMLString::release(&message);
  }
  catch (...) {
    cout << "Excepción no esperada.\n" ;
    return;
  }

  DOMDocument* xmlDoc;
  DOMElement* elementRoot;

  try {
    // Obtener el elemento raíz del documento.
    xmlDoc = parser->getDocument(); 
    elementRoot = xmlDoc->getDocumentElement();

    if(!elementRoot)
      throw(std::runtime_error("Documento XML vacío."));

  }
  catch (xercesc::XMLException& e ) {
    char* message = xercesc::XMLString::transcode( e.getMessage() );
    ostringstream errBuf;
    errBuf << "Error 'parseando': " << message << flush;
    XMLString::release( &message );
    return;
  }

  XMLCh* camera_ch = XMLString::transcode("camera");  
  XMLCh* character_ch = XMLString::transcode("character");
  XMLCh* enemigo_ch = XMLString::transcode("enemigo");
  XMLCh* diamante_ch = XMLString::transcode("diamante");
  XMLCh* pilaVida_ch = XMLString::transcode("pilaVida");
  XMLCh* objetoArma_ch = XMLString::transcode("objetoArma");

  // Procesando los nodos hijos del raíz...
  for (XMLSize_t i = 0; 
       i < elementRoot->getChildNodes()->getLength(); 
       ++i ) {
    
    DOMNode* node = elementRoot->getChildNodes()->item(i);

    if (node->getNodeType() == DOMNode::ELEMENT_NODE) {
      // Nodo <camera>?
      if (XMLString::equals(node->getNodeName(), camera_ch)){
	parseCamera(node, scene);
      } else if (XMLString::equals(node->getNodeName(), character_ch)){
    	  // Nodo <character>?
    	  parseCharacter(node, scene);
      } else if(XMLString::equals(node->getNodeName(), enemigo_ch)){
    	  // Nodo <enemigo>?
    	  parseEnemigo(node, scene);
      } else if(XMLString::equals(node->getNodeName(), diamante_ch)){
    	  // Nodo <diamante>?
    	  parseDiamante(node, scene);
      } else if(XMLString::equals(node->getNodeName(), pilaVida_ch)){
    	  // Nodo <pilaVida>?
    	  parsePilaVida(node, scene);
      } else if(XMLString::equals(node->getNodeName(), objetoArma_ch)){
    	  // Nodo <objetoArma>?
    	  parseObjetoArma(node, scene);
      }
    }
    
  }
  // Liberar recursos.
  XMLString::release(&camera_ch);
  XMLString::release(&character_ch);
  XMLString::release(&enemigo_ch);

  delete parser;
}

void
Importer::parseCamera
(xercesc::DOMNode* cameraNode, Scene* scene)
{
  XMLCh* position_ch = XMLString::transcode("position");
  XMLCh* lookAt_ch = XMLString::transcode("lookAt");

  Vector3 position;
  Vector3 lookAt;

  // Procesar los datos de la cámara.
  for (XMLSize_t i = 0; i < cameraNode->getChildNodes()->getLength(); ++i ) {

    DOMNode* node = cameraNode->getChildNodes()->item(i);

    // Nodo <position>?
    if (node->getNodeType() == DOMNode::ELEMENT_NODE &&
	XMLString::equals(node->getNodeName(), position_ch))
    	getVector(node, &position);


    // Nodo <lookAt>?
    if (node->getNodeType() == DOMNode::ELEMENT_NODE &&
	XMLString::equals(node->getNodeName(), lookAt_ch))
    	getVector(node, &lookAt);

  }

  CameraData *cameraData = new CameraData(position, lookAt);
  scene->addCamera(cameraData);

  XMLString::release(&position_ch);
  XMLString::release(&lookAt_ch);
}

void
Importer::parseCharacter
(xercesc::DOMNode* characterNode, Scene* scene)
{
  XMLCh* position_ch = XMLString::transcode("position");

  Vector3 position;

  for (XMLSize_t i = 0; i < characterNode->getChildNodes()->getLength(); ++i ) {

    DOMNode* node = characterNode->getChildNodes()->item(i);

    if (node->getNodeType() == DOMNode::ELEMENT_NODE) {
      // Nodo <position>?
      if (XMLString::equals(node->getNodeName(), position_ch)){
    	  scene->setCharacterData(new CharacterData);
    	  getVector(node, &position);
    	  scene->getCharacterData()->setPosition(position);
      }

    }

  }

  XMLString::release(&position_ch);
}

void
Importer::parseEnemigo
(xercesc::DOMNode* enemigoNode, Scene* scene)
{
  // Tipo de enemigo
  DOMNamedNodeMap* attributes = enemigoNode->getAttributes();
  DOMNode* typeNode = attributes->getNamedItem(XMLString::transcode("type"));
  DOMNode* enemigoFinal = attributes->getNamedItem(XMLString::transcode("final"));

  int enemigo_type = atoi(XMLString::transcode(typeNode->getNodeValue()));
  int esFinal = atoi(XMLString::transcode(enemigoFinal->getNodeValue()));


  XMLCh* position_ch = XMLString::transcode("position");

  Vector3 position;

  for (XMLSize_t i = 0; i < enemigoNode->getChildNodes()->getLength(); ++i ) {

    DOMNode* node = enemigoNode->getChildNodes()->item(i);

    if (node->getNodeType() == DOMNode::ELEMENT_NODE) {
      // Nodo <position>?
      if (XMLString::equals(node->getNodeName(), position_ch)){
    	  getVector(node, &position);
      }

    }
    
  }

  if (esFinal){
	  EnemigoFinalData *enemigoData = new EnemigoFinalData(position, enemigo_type);
	  scene->setEnemigoFinal(enemigoData);
  } else {
	  EnemigoData *enemigoData = new EnemigoData(position, enemigo_type);
	  scene->addEnemigo(enemigoData);
  }

  XMLString::release(&position_ch);
}

void
Importer::parseDiamante
(xercesc::DOMNode* diamanteNode, Scene* scene)
{
  XMLCh* position_ch = XMLString::transcode("position");

  Vector3 position;

  for (XMLSize_t i = 0; i < diamanteNode->getChildNodes()->getLength(); ++i ) {

    DOMNode* node = diamanteNode->getChildNodes()->item(i);

    if (node->getNodeType() == DOMNode::ELEMENT_NODE) {
      // Nodo <position>?
      if (XMLString::equals(node->getNodeName(), position_ch)){
    	  getVector(node, &position);
      }

    }

  }

  DiamanteData *diamanteData = new DiamanteData(position);
  scene->addDiamante(diamanteData);

  XMLString::release(&position_ch);
}

void
Importer::parsePilaVida
(xercesc::DOMNode* pilaVidaNode, Scene* scene)
{
  XMLCh* position_ch = XMLString::transcode("position");

  Vector3 position;

  for (XMLSize_t i = 0; i < pilaVidaNode->getChildNodes()->getLength(); ++i ) {

    DOMNode* node = pilaVidaNode->getChildNodes()->item(i);

    if (node->getNodeType() == DOMNode::ELEMENT_NODE) {
      // Nodo <position>?
      if (XMLString::equals(node->getNodeName(), position_ch)){
    	  getVector(node, &position);
      }

    }

  }

  PilaVidaData *pilaVidaData = new PilaVidaData(position);
  scene->addPilaVida(pilaVidaData);

  XMLString::release(&position_ch);
}

void
Importer::parseObjetoArma
(xercesc::DOMNode* objetoArmaNode, Scene* scene)
{
  XMLCh* position_ch = XMLString::transcode("position");

  Vector3 position;

  for (XMLSize_t i = 0; i < objetoArmaNode->getChildNodes()->getLength(); ++i ) {

    DOMNode* node = objetoArmaNode->getChildNodes()->item(i);

    if (node->getNodeType() == DOMNode::ELEMENT_NODE) {
      // Nodo <position>?
      if (XMLString::equals(node->getNodeName(), position_ch)){
    	  getVector(node, &position);
      }

    }

  }

  ObjetoArmaData *objetoArmaData = new ObjetoArmaData(position);
  scene->addObjetoArma(objetoArmaData);

  XMLString::release(&position_ch);
}

void
Importer::getVector
(xercesc::DOMNode* node, Vector3* vector)
{
	XMLCh* x_ch = XMLString::transcode("x");
	XMLCh* y_ch = XMLString::transcode("y");
	XMLCh* z_ch = XMLString::transcode("z");

	float x = getValueFromTag(node, x_ch);
	float y = getValueFromTag(node, y_ch);
	float z = getValueFromTag(node, z_ch);

	vector->x = x;
	vector->y = y;
	vector->z = z;

	XMLString::release(&x_ch);
	XMLString::release(&y_ch);
	XMLString::release(&z_ch);
}

float
Importer::getValueFromTag
(xercesc::DOMNode* node, const XMLCh *tag)
{
  for (XMLSize_t i = 0; i < node->getChildNodes()->getLength(); ++i ) {

    DOMNode* aux = node->getChildNodes()->item(i);

    if (aux->getNodeType() == DOMNode::ELEMENT_NODE &&
	XMLString::equals(aux->getNodeName(), tag))
      return atof(XMLString::transcode(aux->getFirstChild()->getNodeValue()));

  }
  return 0.0;
}
