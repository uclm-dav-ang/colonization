#include "../include/RecordsState.h"

#include "IntroState.h"
#include "PlayState.h"

template<> RecordsState* Ogre::Singleton<RecordsState>::msSingleton = 0;

void RecordsState::enter() {
	_root = Ogre::Root::getSingletonPtr();

	// Se recupera el gestor de escena y la cámara.
	_sceneMgr = _root->getSceneManager("SceneManager");
	_camera = _sceneMgr->getCamera("IntroCamera");
	_viewport = _root->getAutoCreatedWindow()->addViewport(_camera);

	Ogre::SceneNode *node_menu = _sceneMgr->createSceneNode("Menu");
	_sceneMgr->getRootSceneNode()->addChild(node_menu);

	//Nodo PlanoMenu
	Ogre::SceneNode *node_PlanoMenu = _sceneMgr->createSceneNode("PlanoMenu");
	Ogre::Entity* ent_PlanoMenu = _sceneMgr->createEntity("PlanoMenu", "PlanoMenu.mesh");
	node_PlanoMenu->attachObject(ent_PlanoMenu);
	node_menu->addChild(node_PlanoMenu);
	node_PlanoMenu->translate(0, 12, 0);
	node_PlanoMenu->yaw(Degree(90));

	getMejoresPuntuaciones();


	_exitGame = false;
}

void RecordsState::exit() {
	_root->getAutoCreatedWindow()->removeAllViewports();
	_sceneMgr->destroyAllCameras();
	_sceneMgr->clearScene();
	_root->destroySceneManager(_sceneMgr);
	Ogre::OverlayManager::getSingletonPtr()->getByName("Puntuacion")->hide();
}

void RecordsState::pause() {
}

void RecordsState::resume() {
}

bool RecordsState::frameStarted(const Ogre::FrameEvent& evt) {
	_overlayManager = Ogre::OverlayManager::getSingletonPtr();
	Ogre::Overlay *overlay = _overlayManager->getByName("Puntuacion");
	overlay->show();

	Ogre::OverlayElement *oe;
	oe = _overlayManager->getOverlayElement("Puntuacion1");
	oe->setCaption(_score[0]);
	oe = _overlayManager->getOverlayElement("Puntuacion2");
	oe->setCaption(_score[1]);
	oe = _overlayManager->getOverlayElement("Puntuacion3");
	oe->setCaption(_score[2]);

	return true;
}

bool RecordsState::frameEnded(const Ogre::FrameEvent& evt) {
	if (_exitGame)
		return false;

	return true;
}

void RecordsState::keyPressed(const OIS::KeyEvent &e) {
}

void RecordsState::keyReleased(const OIS::KeyEvent &e) {
	 if (e.key == OIS::KC_ESCAPE) {
		 changeState(IntroState::getSingletonPtr());
	  }
}

void RecordsState::getMejoresPuntuaciones(){
	std::string mejores[5];
	std::string line;
	std::stringstream fichero;
	int i;

	std::fstream fs;
	//vaciamos la matriz
	int p;
	for (p = 0; p < 3; p++){
		mejores[p] = "";
	}
	fichero.str("");
	fichero << "score.txt";

	fs.open(fichero.str().c_str(), std::fstream::in | std::fstream::out);
	int j;
	i = 0;
	while(getline(fs,line)){
		if(i < 3){
			mejores[i] = line;
			i++;
		} else {
			std::string aux1 = line;
			int numero1, numero2, numero3, posicion1, posicion2, posicion3;
			int menor = 999999999;
			posicion1 = line.find(":");
			numero1 = std::atoi(line.substr(posicion1+1, sizeof(line)).c_str());
			//nos quedamos con el menor
			for (j = 0; j < 3; j++){
				posicion2 = mejores[j].find(":");
				numero2 = std::atoi(mejores[j].substr(posicion2+1, sizeof(mejores[j])).c_str());
				if (numero2 < menor) menor = numero2;
			}
			//buscamos el menor y comparamos con el numero
			for (j = 0; j < 3; j++){
				posicion3 = mejores[j].find(":");
				numero3 = std::atoi(mejores[j].substr(posicion3+1, sizeof(mejores[j])).c_str());
				if(numero3 == menor && numero1 > menor){
					mejores[j] = aux1;
					break;
				}
			}
			i++;
		}
	}
	//ordenamos el array
	int numero0 = std::atoi(mejores[0].substr(mejores[0].find(":")+1, sizeof(mejores[0])).c_str());
	int numero1 = std::atoi(mejores[1].substr(mejores[1].find(":")+1, sizeof(mejores[1])).c_str());
	int numero2 = std::atoi(mejores[2].substr(mejores[2].find(":")+1, sizeof(mejores[2])).c_str());
	std::string aux;

	if (numero0 >= numero1 && numero0 >= numero2){
		if(numero2 > numero1){
			aux = mejores[1];
			mejores[1] = mejores[2];
			mejores[2] = aux;
		}
	} else if (numero1 >= numero0 && numero1 >= numero2) {
		aux = mejores[0];
		mejores[0] = mejores[1];
		mejores[1] = aux;
		if(numero2 > numero0){
			aux = mejores[1];
			mejores[1] = mejores[2];
			mejores[2] = aux;
		}

	} else if (numero2 >= numero0 && numero2 >= numero1) {
		aux = mejores[0];
		mejores[0] = mejores[2];
		mejores[2] = aux;
		if(numero0 > numero1){
			aux = mejores[1];
			mejores[1] = mejores[2];
			mejores[2] = aux;
		}
	}

	//asignamos las puntuaciones
	int l;
	for(l = 0; l < 3; l++){
		_score[l] = mejores[l];
	}

	fs.close();
}

void RecordsState::mouseMoved(const OIS::MouseEvent &e) {
}

void RecordsState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id) {
}

void RecordsState::mouseReleased(const OIS::MouseEvent &e,
		OIS::MouseButtonID id) {
}

RecordsState*
RecordsState::getSingletonPtr() {
	return msSingleton;
}

RecordsState&
RecordsState::getSingleton() {
	assert(msSingleton);
	return *msSingleton;
}
