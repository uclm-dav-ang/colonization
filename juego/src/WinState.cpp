#include "WinState.h"
#include "IntroState.h"
#include "PlayState.h"
#include "ElegirNivelState.h"


template<> WinState* Ogre::Singleton<WinState>::msSingleton = 0;

void WinState::enter() {
	_contadorNombre = 0;
	int i;
	//se vacía el nombre anterior
	for (i = 0; i < int((sizeof(_nombre)/sizeof(_nombre[0]))); i++){
		_nombre[i] = 0;
	}

	_root = Ogre::Root::getSingletonPtr();

	// Se recupera el gestor de escena y la cámara.
	_sceneMgr = _root->getSceneManager("SceneManager");
	_camera = _sceneMgr->getCamera("IntroCamera");
	_camera->setPosition(Ogre::Vector3(0.01, 20, 0));
	_camera->lookAt(Ogre::Vector3(0, 0, 0));
	_viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
	_exitGame = false;
}

void WinState::exit() {
	_root->getAutoCreatedWindow()->removeAllViewports();
	_sceneMgr->destroyAllCameras();
	_sceneMgr->clearScene();
	_root->destroySceneManager(_sceneMgr);

	almacenarPuntuacion();
	Ogre::OverlayManager::getSingletonPtr()->getByName("JuegoPasado")->hide();
}

void WinState::pause() {
}

void WinState::resume() {
}

bool WinState::frameStarted(const Ogre::FrameEvent& evt) {
	_overlayManager = Ogre::OverlayManager::getSingletonPtr();
	Ogre::Overlay *overlay = _overlayManager->getByName("JuegoPasado");
	overlay->show();

	Ogre::OverlayElement *oe;
	oe = _overlayManager->getOverlayElement("NombreTeclado");
	oe->setCaption(_nombre);
	return true;
}

bool WinState::frameEnded(const Ogre::FrameEvent& evt) {
	if (_exitGame)
		return false;

	return true;
}

void WinState::keyPressed(const OIS::KeyEvent &e) {
	// Tecla p --> Estado anterior.
	if (e.key == 28 && _nombre[0]) {
		int i;
		std::vector<bool> nivelesConseguidos;
		for (i = 0; i < 3; i++){
			nivelesConseguidos.push_back(false);
		}
		ElegirNivelState::getSingletonPtr()->setNivelesConseguidos(nivelesConseguidos);
		PlayState::getSingletonPtr()->setPuntos(0);
		changeState(IntroState::getSingletonPtr());
	}

	//se rellena el nombre
	if (((16 <= e.key && e.key <= 25) || (30 <= e.key && e.key <= 38) || (44 <= e.key && e.key <= 50)) && (_contadorNombre < 3)){
		char letra = int(e.text);
		_nombre[_contadorNombre] = letra;
		_contadorNombre++;
	}

	if (e.key == OIS::KC_BACK && _contadorNombre > 0){
		_contadorNombre--;
		_nombre[_contadorNombre] = 0;
	}
}

void WinState::almacenarPuntuacion(){
	char *fichero = "score.txt";

	std::fstream fs;
	fs.open(fichero, std::fstream::in | std::fstream::out | std::fstream::ate);
	fs << _nombre << ": " << PlayState::getSingletonPtr()->getPuntuacion() << endl;
	fs.close();
}

void WinState::keyReleased(const OIS::KeyEvent &e) {
}

void WinState::mouseMoved(const OIS::MouseEvent &e) {
}

void WinState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id) {
}

void WinState::mouseReleased(const OIS::MouseEvent &e,
		OIS::MouseButtonID id) {
}

WinState*
WinState::getSingletonPtr() {
	return msSingleton;
}

WinState&
WinState::getSingleton() {
	assert(msSingleton);
	return *msSingleton;
}
