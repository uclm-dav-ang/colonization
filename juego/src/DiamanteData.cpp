#include <DiamanteData.h>
#include <OgreSceneNode.h>
#include <cmath>

DiamanteData::DiamanteData
(Ogre::Vector3 position)
{
	_position = position;
}

DiamanteData::~DiamanteData ()
{
}

Vector3 DiamanteData::getPosition()const {
	return _position;
}

void DiamanteData::setPosition(Vector3 position){
	_position = position;
}
