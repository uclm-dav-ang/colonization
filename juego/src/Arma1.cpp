#include <Arma1.h>
#include <OgreSceneNode.h>
#include <cmath>

Arma1::Arma1(Enemigo* enemigoObjetivo, String nombre, Robot *robot) : Arma(enemigoObjetivo, nombre, robot) {
	_danio = 25;

	_nodeArma = _sceneMgr->createSceneNode(nombre);
	Ogre::Entity* entArma = _sceneMgr->createEntity(nombre, "Disparo1.mesh");
	_nodeArma->attachObject(entArma);
	_sceneMgr->getRootSceneNode()->addChild(_nodeArma);
	SceneNode *nodo = _robot->getNode();
	Bone* bone = _sceneMgr->getEntity(nodo->getName())->getSkeleton()->getBone("ManoDerecha");
	Vector3 posicion = nodo->_getDerivedPosition() + nodo->_getDerivedOrientation() * bone->_getDerivedPosition();
	_nodeArma->setPosition(posicion);

	//particula del arma
	std::stringstream saux;
	std::string s = "particle";
	saux << nombre << s;
	Ogre::ParticleSystem* ps = _sceneMgr->createParticleSystem(saux.str(),"disparo1");
	Ogre::SceneNode* psNode = _sceneMgr->createSceneNode(saux.str());
	psNode->attachObject(ps);

	_nodeArma->addChild(psNode);
}

Arma1::Arma1(const Arma1 &obj) : Arma (obj){
	_nodeArma = obj.getNode();
	_enemigoObjetivo = obj.getEnemigoObjetivo();
	_robot = obj.getRobot();

	_danio = 25;
	_contador = 2000;
	_m_precisionTimer = new Ogre::Timer();


	_root = Ogre::Root::getSingletonPtr();
	_sceneMgr = _root->getSceneManager("SceneManager");
}

Arma1::~Arma1() {
	delete _nodeArma;
	delete _enemigoObjetivo;
}

Arma1& Arma1::operator= (const Arma1 &obj){
	delete _nodeArma;
	delete _enemigoObjetivo;
	delete _robot;
	_nodeArma = obj._nodeArma;
	_enemigoObjetivo = obj._enemigoObjetivo;

	return *this;
}
