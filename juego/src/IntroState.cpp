#include "IntroState.h"

#include "../include/RecordsState.h"
#include "ElegirNivelState.h"
#include "InstruccionesState.h"
#include "../include/CreditosState.h"

template<> IntroState* Ogre::Singleton<IntroState>::msSingleton = 0;

void
IntroState::enter ()
{
  _opcionesMenu.clear();
  _vector_animacionMenu.clear();

  _root = Ogre::Root::getSingletonPtr();

  if (!_root->hasSceneManager("SceneManager")){
	  _sceneMgr = _root->createSceneManager(Ogre::ST_GENERIC, "SceneManager");
  } else {
	  _sceneMgr = _root->getSceneManager("SceneManager");
  }
  if (!_sceneMgr->hasCamera("IntroCamera")){
	  _camera = _sceneMgr->createCamera("IntroCamera");
  } else {
	  _camera = _sceneMgr->getCamera("IntroCamera");
  }
  _camera->setPosition(Ogre::Vector3(0.01, 20, 0));
  _camera->lookAt(Ogre::Vector3(0, 0, 0));
  _camera->setNearClipDistance(1);
  //_camera->setFarClipDistance(100);
  _camera->setFOVy(Ogre::Degree(50));
  _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);

  crearMenu();

  inicializarAnimaciones();

  _menuSeleccion = 0;
  cambiarTexturaMenu();

  _exitGame = false;
}

void
IntroState::exit()
{
  _sceneMgr->clearScene();
  _root->getAutoCreatedWindow()->removeAllViewports();
  _sceneMgr->destroyAllAnimationStates();
}

void
IntroState::pause ()
{
}

void
IntroState::resume ()
{
}

bool
IntroState::frameStarted
(const Ogre::FrameEvent& evt) 
{
  Ogre::Real deltaT = evt.timeSinceLastFrame;
  //animaciones del menu
  int i;
  for(i = 0; i < NUMERO_MENU_OPCIONES; i++){
	  if (_vector_animacionMenu[i]->getEnabled() && !_vector_animacionMenu[i]->hasEnded()){
		 _vector_animacionMenu[i]->addTime(deltaT);
	  }

	  if(_vector_animacionMenu[i]->hasEnded() && _vector_animacionMenu[i]->getEnabled()){
		  _vector_animacionMenu[i]->setEnabled(false);
		  _vector_animacionMenu[i]->setLoop(false);
	  }
  }


  if (_vector_animacionMenu[NUMERO_MENU_OPCIONES-1]->hasEnded()){
	  cambiarTexturaMenu();
  }


  return true;
}

bool
IntroState::frameEnded
(const Ogre::FrameEvent& evt)
{
  if (_exitGame)
    return false;
  
  return true;
}

void
IntroState::keyPressed
(const OIS::KeyEvent &e)
{
  //transicion al PlayState
  if (e.key == 28 && _menuSeleccion == 0) {
    changeState(ElegirNivelState::getSingletonPtr());
  }

  if (e.key == 28 && _menuSeleccion == 1 && _vector_animacionMenu[0]->hasEnded() && !_vector_animacionMenu[0]->getEnabled()) {
	  changeState(InstruccionesState::getSingletonPtr());
  }

  if (e.key == 28 && _menuSeleccion == 2 && _vector_animacionMenu[0]->hasEnded() && !_vector_animacionMenu[0]->getEnabled()) {
	  changeState(RecordsState::getSingletonPtr());
  }

  if (e.key == 28 && _menuSeleccion == 3 && _vector_animacionMenu[0]->hasEnded() && !_vector_animacionMenu[0]->getEnabled()) {
  	  changeState(CreditosState::getSingletonPtr());
    }

  if (e.key == 28 && _menuSeleccion == 4 && _vector_animacionMenu[0]->hasEnded() && !_vector_animacionMenu[0]->getEnabled()) {
      _exitGame = true;
    }

  if (e.key == OIS::KC_UP && !_vector_animacionMenu[NUMERO_MENU_OPCIONES-1]->getEnabled()){
	  getDownAnimations();
	  _menuSeleccion--;
	  animarMenu();

	  if(_menuSeleccion == -1){
		  _menuSeleccion = NUMERO_MENU_OPCIONES-1;
	  }
  }

  if (e.key == OIS::KC_DOWN && !_vector_animacionMenu[NUMERO_MENU_OPCIONES-1]->getEnabled()){
	  getUpAnimations();
	  _menuSeleccion++;
	  animarMenu();

	  if(_menuSeleccion == NUMERO_MENU_OPCIONES){
		  _menuSeleccion = 0;
	  }
  }
}

void
IntroState::keyReleased
(const OIS::KeyEvent &e )
{
}

void
IntroState::mouseMoved
(const OIS::MouseEvent &e)
{
}

void
IntroState::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
}

void
IntroState::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
}

void IntroState::crearMenu(){
	Ogre::SceneNode *node_menu = _sceneMgr->createSceneNode("Menu");
	_sceneMgr->getRootSceneNode()->addChild(node_menu);

	//Nodo PlanoMenu
	Ogre::SceneNode *node_PlanoMenu = _sceneMgr->createSceneNode("PlanoMenu");
	Ogre::Entity* ent_PlanoMenu = _sceneMgr->createEntity("PlanoMenu", "PlanoMenu.mesh");
	node_PlanoMenu->attachObject(ent_PlanoMenu);
	node_menu->addChild(node_PlanoMenu);
	node_PlanoMenu->translate(0, 12, 0);
	node_PlanoMenu->yaw(Degree(90));

	//Nodo Jugar
	Ogre::SceneNode *node_jugar = _sceneMgr->createSceneNode("Menu0");
	Ogre::Entity* ent_jugar = _sceneMgr->createEntity("Menu0", "Plane.mesh");
	node_jugar->attachObject(ent_jugar);
	node_menu->addChild(node_jugar);
	node_jugar->translate(0, 0, -5);
	node_jugar->yaw(Ogre::Degree(90));
	_opcionesMenu.push_back(node_jugar);

	//Nodo Instrucciones
	Ogre::SceneNode *node_instrucciones = _sceneMgr->createSceneNode("Menu1");
	Ogre::Entity* ent_instrucciones = _sceneMgr->createEntity("Menu1", "Plane.mesh");
	node_instrucciones->attachObject(ent_instrucciones);
	node_menu->addChild(node_instrucciones);
	node_instrucciones->translate(0, 0, 5);
	node_instrucciones->yaw(Ogre::Degree(90));
	ent_instrucciones->setMaterialName("MaterialMenu2");
	_opcionesMenu.push_back(node_instrucciones);

	//Nodo Records
	Ogre::SceneNode *node_records = _sceneMgr->createSceneNode("Menu2");
	Ogre::Entity* ent_records = _sceneMgr->createEntity("Menu2", "Plane.mesh");
	node_records->attachObject(ent_records);
	node_menu->addChild(node_records);
	node_records->translate(-5, 0, 5);
	node_records->yaw(Ogre::Degree(90));
	ent_records->setMaterialName("MaterialMenu3");
	_opcionesMenu.push_back(node_records);

	//Nodo Creditos
	Ogre::SceneNode *node_creditos = _sceneMgr->createSceneNode("Menu3");
	Ogre::Entity* ent_creditos = _sceneMgr->createEntity("Menu3", "Plane.mesh");
	node_creditos->attachObject(ent_creditos);
	node_menu->addChild(node_creditos);
	node_creditos->translate(20, 0, 5);
	node_creditos->yaw(Ogre::Degree(90));
	ent_creditos->setMaterialName("MaterialMenu4");
	_opcionesMenu.push_back(node_creditos);

	//Nodo Salir
	Ogre::SceneNode *node_salir = _sceneMgr->createSceneNode("Menu4");
	Ogre::Entity* ent_salir = _sceneMgr->createEntity("Menu4", "Plane.mesh");
	node_salir->attachObject(ent_salir);
	node_menu->addChild(node_salir);
	node_salir->translate(5, 0, 5);
	node_salir->yaw(Ogre::Degree(90));
	ent_salir->setMaterialName("MaterialMenu5");
	_opcionesMenu.push_back(node_salir);

	//Nodo MenuSeleccion
	Ogre::SceneNode *node_MenuSeleccion = _sceneMgr->createSceneNode("MenuSeleccion");
	Ogre::Entity* ent_MenuSeleccion = _sceneMgr->createEntity("MenuSeleccion", "MenuSeleccion.mesh");
	node_MenuSeleccion->attachObject(ent_MenuSeleccion);
	node_menu->addChild(node_MenuSeleccion);
	node_MenuSeleccion->translate(0, 0, 5);
	node_MenuSeleccion->yaw(Ogre::Degree(90));
	ent_MenuSeleccion->setMaterialName("MaterialMenuSeleccion");
}

void IntroState::inicializarAnimaciones(){
	int i;
	for (i = 0; i < NUMERO_MENU_OPCIONES; i++){
		Ogre::AnimationState *animation;
		animation = _sceneMgr->getEntity(_opcionesMenu[i]->getName())->getAnimationState("downSelected");
		animation->setEnabled(false);
		_vector_animacionMenu.push_back(animation);
	}
}

void IntroState::animarMenu(){
	int i;
	for (i = 0; i < NUMERO_MENU_OPCIONES; i++){
		_vector_animacionMenu[i]->setTimePosition(0.0);
		_vector_animacionMenu[i]->setEnabled(true);
		_vector_animacionMenu[i]->setLoop(false);
	}
}



void IntroState::cambiarTexturaMenu(){
	int i;
	std::stringstream materialSeleccionadoName;
	std::string s = "MaterialMenu";
	for (i = 0; i < NUMERO_MENU_OPCIONES; i++) {
		_opcionesMenu[i]->setScale(1, 1, 1);
		if (_menuSeleccion == 0 && (i == NUMERO_MENU_OPCIONES-1 || (i == NUMERO_MENU_OPCIONES-2))) {
			_sceneMgr->getEntity(_opcionesMenu[i]->getName())->setVisibilityFlags(true);
			materialSeleccionadoName << s << i+1;
			//_sceneMgr->getEntity(_opcionesMenu[i]->getName())->setMaterialName(materialSeleccionadoName.str());
			if (i == NUMERO_MENU_OPCIONES-1){
				_sceneMgr->getSceneNode(_opcionesMenu[i]->getName())->setPosition(-5, 0, 5);
			} else {
				_sceneMgr->getSceneNode(_opcionesMenu[i]->getName())->setPosition(-10, 0, 5);
			}
			_opcionesMenu[i]->setScale(1, 1, 1);

		} else if (_menuSeleccion == (NUMERO_MENU_OPCIONES-1) && (i == 0 || i == 1)) {
			_sceneMgr->getEntity(_opcionesMenu[i]->getName())->setVisibilityFlags(true);
			materialSeleccionadoName << s << i+1;
			//_sceneMgr->getEntity(_opcionesMenu[i]->getName())->setMaterialName(materialSeleccionadoName.str());
			if(i == 0){
				_sceneMgr->getSceneNode(_opcionesMenu[i]->getName())->setPosition(5, 0, 5);
			} else {
				_sceneMgr->getSceneNode(_opcionesMenu[i]->getName())->setPosition(10, 0, 5);
			}
			_opcionesMenu[i]->setScale(1, 1, 1);

		} else if (_menuSeleccion == (NUMERO_MENU_OPCIONES-2) && i == 0) {
			_sceneMgr->getEntity(_opcionesMenu[i]->getName())->setVisibilityFlags(true);
			materialSeleccionadoName << s << i+1;
			_sceneMgr->getSceneNode(_opcionesMenu[i]->getName())->setPosition(10, 0, 5);
			_opcionesMenu[i]->setScale(1, 1, 1);

		} else if (_menuSeleccion == 1 && i == NUMERO_MENU_OPCIONES-1) {
			_sceneMgr->getEntity(_opcionesMenu[i]->getName())->setVisibilityFlags(true);
			materialSeleccionadoName << s << i+1;
			_sceneMgr->getSceneNode(_opcionesMenu[i]->getName())->setPosition(-10, 0, 5);
			_opcionesMenu[i]->setScale(1, 1, 1);

		}

		else if (_menuSeleccion == i){
			_sceneMgr->getEntity(_opcionesMenu[i]->getName())->setVisibilityFlags(true);
			materialSeleccionadoName << s << i+1;
			//_sceneMgr->getEntity(_opcionesMenu[i]->getName())->setMaterialName(materialSeleccionadoName.str());
			_sceneMgr->getSceneNode(_opcionesMenu[i]->getName())->setPosition(0, 0, 5);
			_opcionesMenu[i]->setScale(1.25, 1, 1); // la opcion seleccionada mas grande

		} else if (abs(_menuSeleccion-i) == 1) {
			_sceneMgr->getEntity(_opcionesMenu[i]->getName())->setVisibilityFlags(true);
			materialSeleccionadoName << s << i+1;
			//_sceneMgr->getEntity(_opcionesMenu[i]->getName())->setMaterialName(materialSeleccionadoName.str());

			if (_menuSeleccion > i) {
				_sceneMgr->getSceneNode(_opcionesMenu[i]->getName())->setPosition(-5, 0, 5);
			} else {
				_sceneMgr->getSceneNode(_opcionesMenu[i]->getName())->setPosition(5, 0, 5);
			}
			_opcionesMenu[i]->setScale(1, 1, 1);

		} else if (abs(_menuSeleccion-i) == 2) {
			_sceneMgr->getEntity(_opcionesMenu[i]->getName())->setVisibilityFlags(true);
			materialSeleccionadoName << s << i+1;
			//_sceneMgr->getEntity(_opcionesMenu[i]->getName())->setMaterialName(materialSeleccionadoName.str());

			if (_menuSeleccion > i) {
				_sceneMgr->getSceneNode(_opcionesMenu[i]->getName())->setPosition(-10, 0, 5);
			} else {
				_sceneMgr->getSceneNode(_opcionesMenu[i]->getName())->setPosition(10, 0, 5);
			}
			_opcionesMenu[i]->setScale(1, 1, 1);

		} else {
			_sceneMgr->getEntity(_opcionesMenu[i]->getName())->setVisibilityFlags(false);
		}

		materialSeleccionadoName.str("");
	}





	/*
	std::stringstream materialSeleccionadoName;
	std::string s = "MaterialMenuM";
	materialSeleccionadoName << s << _menuSeleccion+1;

	std::stringstream materialNoSeleccionadoName;
	std::string s2 = "MaterialMenu";
	materialNoSeleccionadoName << s2 << _menuSeleccion+1;

	_sceneMgr->getEntity(_opcionesMenu[_menuSeleccion]->getName())->setMaterialName(materialSeleccionadoName.str());
	_sceneMgr->getSceneNode(_opcionesMenu[_menuSeleccion]->getName())->setPosition(0, 0, 0);

	if(_menuSeleccion == 0){
		materialSeleccionadoName.str("");
		materialSeleccionadoName << s2 << "4";
		_sceneMgr->getEntity(_opcionesMenu[3]->getName())->setMaterialName(materialSeleccionadoName.str());
		_sceneMgr->getSceneNode(_opcionesMenu[3]->getName())->setPosition(-5, 0, 0);

		materialSeleccionadoName.str("");
		materialSeleccionadoName << s2 << "2";
		_sceneMgr->getEntity(_opcionesMenu[1]->getName())->setMaterialName(materialSeleccionadoName.str());
		_sceneMgr->getSceneNode(_opcionesMenu[1]->getName())->setPosition(5, 0, 0);
	} else if (_menuSeleccion == 3){
		materialSeleccionadoName.str("");
		materialSeleccionadoName << s2 << "3";
		_sceneMgr->getEntity(_opcionesMenu[2]->getName())->setMaterialName(materialSeleccionadoName.str());
		_sceneMgr->getSceneNode(_opcionesMenu[2]->getName())->setPosition(-5, 0, 0);

		materialSeleccionadoName.str("");
		materialSeleccionadoName << s2 << "1";
		_sceneMgr->getEntity(_opcionesMenu[0]->getName())->setMaterialName(materialSeleccionadoName.str());
		_sceneMgr->getSceneNode(_opcionesMenu[0]->getName())->setPosition(5, 0, 0);
	} else {
		materialSeleccionadoName.str("");
		materialSeleccionadoName << s2 << _menuSeleccion;
		_sceneMgr->getEntity(_opcionesMenu[_menuSeleccion-1]->getName())->setMaterialName(materialSeleccionadoName.str());
		_sceneMgr->getSceneNode(_opcionesMenu[_menuSeleccion-1]->getName())->setPosition(-5, 0, 0);

		materialSeleccionadoName.str("");
		materialSeleccionadoName << s2 << _menuSeleccion+2;
		_sceneMgr->getEntity(_opcionesMenu[_menuSeleccion+1]->getName())->setMaterialName(materialSeleccionadoName.str());
		_sceneMgr->getSceneNode(_opcionesMenu[_menuSeleccion+1]->getName())->setPosition(5, 0, 0);
	}

	materialSeleccionadoName.str("");
	materialNoSeleccionadoName.str("");*/

	/*switch (_menuSeleccion){
	case 0:
		_sceneMgr->getEntity("Jugar")->setMaterialName("MaterialMenuM1");
		_sceneMgr->getEntity("Records")->setMaterialName("MaterialMenu2");

		_sceneMgr->getSceneNode("Jugar")->setPosition(0, 0, 0);
		_sceneMgr->getSceneNode("Records")->setPosition(-5, 0, 0);
		_sceneMgr->getSceneNode("Salir")->setPosition(5, 0, 0);
		break;
	case 1:
		_sceneMgr->getEntity("Records")->setMaterialName("MaterialMenuM2");
		_sceneMgr->getEntity("Jugar")->setMaterialName("MaterialMenu1");
		_sceneMgr->getEntity("Creditos")->setMaterialName("MaterialMenu3");

		_sceneMgr->getSceneNode("Records")->setPosition(0, 0, 0);
		_sceneMgr->getSceneNode("Jugar")->setPosition(5, 0, 0);
		_sceneMgr->getSceneNode("Salir")->setPosition(-5, 0, 0);
		break;
	case 2:
		_sceneMgr->getEntity("Creditos")->setMaterialName("MaterialMenuM3");
		_sceneMgr->getEntity("Records")->setMaterialName("MaterialMenu2");
		_sceneMgr->getEntity("Salir")->setMaterialName("MaterialMenu4");
		break;
	case 3:
		_sceneMgr->getEntity("Salir")->setMaterialName("MaterialMenuM4");
		_sceneMgr->getEntity("Creditos")->setMaterialName("MaterialMenu3");

		_sceneMgr->getSceneNode("Salir")->setPosition(0, 0, 0);
		_sceneMgr->getSceneNode("Records")->setPosition(5, 0, 0);
		_sceneMgr->getSceneNode("Jugar")->setPosition(-5, 0, 0);
		break;
	}*/
}

void IntroState::getDownAnimations(){
	int i;
		for (i = 0; i < NUMERO_MENU_OPCIONES; i++) {
			if (_menuSeleccion == 0 && (i == NUMERO_MENU_OPCIONES-1 || (i == NUMERO_MENU_OPCIONES-2))) {
				if (i == NUMERO_MENU_OPCIONES-1){
					_vector_animacionMenu[i] = _sceneMgr->getEntity(_opcionesMenu[i]->getName())->getAnimationState("downSelected");
				} else {
					_vector_animacionMenu[i] = _sceneMgr->getEntity(_opcionesMenu[i]->getName())->getAnimationState("down");
				};

			} else if (_menuSeleccion == (NUMERO_MENU_OPCIONES-1) && (i == 0 || i == 1)) {
				_vector_animacionMenu[i] = _sceneMgr->getEntity(_opcionesMenu[i]->getName())->getAnimationState("down");

			} else if (_menuSeleccion == (NUMERO_MENU_OPCIONES-2) && i == 0) {
				_vector_animacionMenu[i] = _sceneMgr->getEntity(_opcionesMenu[i]->getName())->getAnimationState("down");

			} else if (_menuSeleccion == 1 && i == NUMERO_MENU_OPCIONES-1) {
				_vector_animacionMenu[i] = _sceneMgr->getEntity(_opcionesMenu[i]->getName())->getAnimationState("down");

			}

			else if (_menuSeleccion == i){
				_vector_animacionMenu[i] = _sceneMgr->getEntity(_opcionesMenu[i]->getName())->getAnimationState("downNoSelected");
				_opcionesMenu[i]->setScale(1, 1, 1);

			} else if (abs(_menuSeleccion-i) == 1) {
				if (_menuSeleccion > i) {
					_vector_animacionMenu[i] = _sceneMgr->getEntity(_opcionesMenu[i]->getName())->getAnimationState("downSelected");
				} else {
					_vector_animacionMenu[i] = _sceneMgr->getEntity(_opcionesMenu[i]->getName())->getAnimationState("down");
				}

			} else if (abs(_menuSeleccion-i) == 2) {
				_vector_animacionMenu[i] = _sceneMgr->getEntity(_opcionesMenu[i]->getName())->getAnimationState("down");

			} else {
				_vector_animacionMenu[i] = _sceneMgr->getEntity(_opcionesMenu[i]->getName())->getAnimationState("down");
			}
		}
}

void IntroState::getUpAnimations(){
	int i;
		for (i = 0; i < NUMERO_MENU_OPCIONES; i++) {
			if (_menuSeleccion == 0 && (i == NUMERO_MENU_OPCIONES-1 || (i == NUMERO_MENU_OPCIONES-2))) {
				if (i == NUMERO_MENU_OPCIONES-1){
					_vector_animacionMenu[i] = _sceneMgr->getEntity(_opcionesMenu[i]->getName())->getAnimationState("up");
				} else {
					_vector_animacionMenu[i] = _sceneMgr->getEntity(_opcionesMenu[i]->getName())->getAnimationState("up");
				};

			} else if (_menuSeleccion == (NUMERO_MENU_OPCIONES-1) && (i == 0 || i == 1)) {
				if (i == 0){
					_vector_animacionMenu[i] = _sceneMgr->getEntity(_opcionesMenu[i]->getName())->getAnimationState("upSelected");
				} else {
					_vector_animacionMenu[i] = _sceneMgr->getEntity(_opcionesMenu[i]->getName())->getAnimationState("up");
				}

			} else if (_menuSeleccion == (NUMERO_MENU_OPCIONES-2) && i == 0) {
				_vector_animacionMenu[i] = _sceneMgr->getEntity(_opcionesMenu[i]->getName())->getAnimationState("up");

			} else if (_menuSeleccion == 1 && i == NUMERO_MENU_OPCIONES-1) {
				_vector_animacionMenu[i] = _sceneMgr->getEntity(_opcionesMenu[i]->getName())->getAnimationState("up");

			}

			else if (_menuSeleccion == i){
				_vector_animacionMenu[i] = _sceneMgr->getEntity(_opcionesMenu[i]->getName())->getAnimationState("upNoSelected");
				_opcionesMenu[i]->setScale(1, 1, 1);

			} else if (abs(_menuSeleccion-i) == 1) {
				if (_menuSeleccion > i) {
					_vector_animacionMenu[i] = _sceneMgr->getEntity(_opcionesMenu[i]->getName())->getAnimationState("up");
				} else {
					_vector_animacionMenu[i] = _sceneMgr->getEntity(_opcionesMenu[i]->getName())->getAnimationState("upSelected");
				}

			} else if (abs(_menuSeleccion-i) == 2) {
				_vector_animacionMenu[i] = _sceneMgr->getEntity(_opcionesMenu[i]->getName())->getAnimationState("up");

			} else {
				_vector_animacionMenu[i] = _sceneMgr->getEntity(_opcionesMenu[i]->getName())->getAnimationState("up");
			}
		}
}

IntroState*
IntroState::getSingletonPtr ()
{
return msSingleton;
}

IntroState&
IntroState::getSingleton ()
{ 
  assert(msSingleton);
  return *msSingleton;
}
