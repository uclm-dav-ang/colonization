#define UNUSED_VARIABLE(x) (void)x

#include "GameManager.h"
#include "IntroState.h"
#include "PlayState.h"
#include "PauseState.h"
#include "LoseState.h"
#include "RecordsState.h"
#include "CreditosState.h"
#include "ElegirNivelState.h"
#include "InstruccionesState.h"
#include "WinState.h"
#include "NivelSuperadoState.h"

#include <iostream>

using namespace std;

int main () {

  GameManager* game = new GameManager();
  IntroState* introState = new IntroState();
  PlayState* playState = new PlayState();
  PauseState* pauseState = new PauseState();
  LoseState* loseState = new LoseState();
  RecordsState* recordsState = new RecordsState();
  CreditosState* creditosState = new CreditosState();
  ElegirNivelState* elegirNivelState = new ElegirNivelState();
  InstruccionesState* instruccionesState = new InstruccionesState();
  WinState* winState = new WinState();
  NivelSuperadoState* nivelSuperadoState = new NivelSuperadoState();


  UNUSED_VARIABLE(introState);
  UNUSED_VARIABLE(playState);
  UNUSED_VARIABLE(pauseState);
  UNUSED_VARIABLE(loseState);
  UNUSED_VARIABLE(recordsState);
  UNUSED_VARIABLE(creditosState);
  UNUSED_VARIABLE(elegirNivelState);
  UNUSED_VARIABLE(instruccionesState);
  UNUSED_VARIABLE(winState);
  UNUSED_VARIABLE(nivelSuperadoState);

  try
    {
      // Inicializa el juego y transición al primer estado.
      game->start(IntroState::getSingletonPtr());
    }
  catch (Ogre::Exception& e)
    {
      std::cerr << "Excepción detectada: " << e.getFullDescription();
    }
  
  delete game;
  
  return 0;
}
