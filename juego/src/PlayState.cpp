#include "PlayState.h"
#include "PauseState.h"
#include "LoseState.h"
#include "IntroState.h"
#include "LoseState.h"
#include "WinState.h"
#include "ElegirNivelState.h"
#include "NivelSuperadoState.h"

#include "MeshStrider.h"
#include <Importer.h>

template<> PlayState* Ogre::Singleton<PlayState>::msSingleton = 0;
void PlayState::enter() {
	_allEnemiesDead = false;
	_diamantesConseguidos = 0;
	_enemigoFinal = NULL;

	//overlays
	_overlayManager = Ogre::OverlayManager::getSingletonPtr();
	Ogre::Overlay *overlayBarra = _overlayManager->getByName("Objetos");
	overlayBarra->show();

	Ogre::Overlay *overlayVida = _overlayManager->getByName("VidaRobot");
	overlayVida->show();

	Ogre::Overlay *overlayMenuArmas = _overlayManager->getByName("MenuArmas");
	overlayMenuArmas->show();

	Ogre::Overlay *overlayPuntos = _overlayManager->getByName("Puntos");
	overlayPuntos->show();

	_overlayArmaSeleccionada1 = _overlayManager->getByName("ArmaSeleccionada1");
	_overlayArmaSeleccionada2 = _overlayManager->getByName("ArmaSeleccionada2");
	_overlayArmaSeleccionada3 = _overlayManager->getByName("ArmaSeleccionada3");

	_mover = false;
	_girarIzquierda = false;
	_girarDerecha = false;

	parsear_escenario();

	_root = Ogre::Root::getSingletonPtr();

	// Se recupera el gestor de escena y la cámara.
	_sceneMgr = _root->getSceneManager("SceneManager");
	//Creamos el nodo principal del que colgarán los demás
	_node_principal = _sceneMgr->createSceneNode("principal");
	_sceneMgr->getRootSceneNode()->addChild(_node_principal);


	_sceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);

	std::vector<CameraData*> camerasData;
	camerasData = _scene->getCameras();

	_camera = _sceneMgr->getCamera("IntroCamera");
  	_camera->setNearClipDistance(1);
  	//_camera->setFarClipDistance(500);
	_viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
	_viewport->setBackgroundColour(Ogre::ColourValue(0.0, 0.5, 1.0));

	crearLuz();

	_broadphase = new btDbvtBroadphase();
	_collisionConf = new btDefaultCollisionConfiguration();
	_dispatcher = new btCollisionDispatcher(_collisionConf);
	_solver = new btSequentialImpulseConstraintSolver;
	_world = new btDiscreteDynamicsWorld(_dispatcher, _broadphase, _solver,
			_collisionConf);

	// Establecimiento propiedades del mundo
	_world->setGravity(btVector3(0, -10, 0));

	// Creacion de los elementos iniciales del mundo
	CreateInitialWorld();

	_diamantesTotales = _robot->getDiamantes().size();

	_exCamera = new ExtendedCamera("Extended Camera", _sceneMgr, _camera);
	_exCamera->instantUpdate(_robot->getCameraNode()->_getDerivedPosition(), _robot->getSightNode()->_getDerivedPosition());


	_exitGame = false;
}

void PlayState::exit() {
	Ogre::OverlayManager::getSingletonPtr()->getByName("VidaRobot")->hide();
	Ogre::OverlayManager::getSingletonPtr()->getByName("Objetos")->hide();
	Ogre::OverlayManager::getSingletonPtr()->getByName("MenuArmas")->hide();
	Ogre::OverlayManager::getSingletonPtr()->getByName("Puntos")->hide();
	Ogre::OverlayManager::getSingletonPtr()->getByName("Arma1")->hide();
	Ogre::OverlayManager::getSingletonPtr()->getByName("Arma2")->hide();
	Ogre::OverlayManager::getSingletonPtr()->getByName("Arma3")->hide();
	Ogre::OverlayManager::getSingletonPtr()->getByName("ArmaSeleccionada1")->hide();
	Ogre::OverlayManager::getSingletonPtr()->getByName("ArmaSeleccionada2")->hide();
	Ogre::OverlayManager::getSingletonPtr()->getByName("ArmaSeleccionada3")->hide();

	_sceneMgr->clearScene();
	_root->getAutoCreatedWindow()->removeAllViewports();
}

void PlayState::pause() {
	_robot->setAvanzar(false);
	_robot->setGirarIzquierda(false);
	_robot->setGirarDerecha(false);

	_robot->pararAvance();
	_robot->pararGiro();

	if (_sceneMgr->hasLight("Light"))
		_sceneMgr->destroyLight("Light");

	Ogre::OverlayManager::getSingletonPtr()->getByName("VidaRobot")->hide();
	Ogre::OverlayManager::getSingletonPtr()->getByName("Objetos")->hide();
	Ogre::OverlayManager::getSingletonPtr()->getByName("MenuArmas")->hide();
	Ogre::OverlayManager::getSingletonPtr()->getByName("Puntos")->hide();
	Ogre::OverlayManager::getSingletonPtr()->getByName("Arma1")->hide();
	Ogre::OverlayManager::getSingletonPtr()->getByName("Arma2")->hide();
	Ogre::OverlayManager::getSingletonPtr()->getByName("Arma3")->hide();
	Ogre::OverlayManager::getSingletonPtr()->getByName("ArmaSeleccionada1")->hide();
	Ogre::OverlayManager::getSingletonPtr()->getByName("ArmaSeleccionada2")->hide();
	Ogre::OverlayManager::getSingletonPtr()->getByName("ArmaSeleccionada3")->hide();
}

void PlayState::resume() {
	crearLuz();

	// Se restaura el background colour.
	_viewport->setBackgroundColour(Ogre::ColourValue(0.0, 0.5, 1.0));
	_viewport->setCamera(_camera);

	Ogre::OverlayManager::getSingletonPtr()->getByName("VidaRobot")->show();
	Ogre::OverlayManager::getSingletonPtr()->getByName("MenuArmas")->show();
	Ogre::OverlayManager::getSingletonPtr()->getByName("Objetos")->show();
	Ogre::OverlayManager::getSingletonPtr()->getByName("Puntos")->show();

	std::vector<bool> armasconseguidas = _robot->getArmasConseguidas();
	if (armasconseguidas[0])
		Ogre::OverlayManager::getSingletonPtr()->getByName("Arma1")->show();
	if (armasconseguidas[1])
		Ogre::OverlayManager::getSingletonPtr()->getByName("Arma2")->show();
	if (armasconseguidas[2])
		Ogre::OverlayManager::getSingletonPtr()->getByName("Arma3")->show();

	switch(_robot->getArmaSeleccionada()){
	case 1:
		mostrarOverlayArmaConseguidaSeleccionada1();
		break;
	case 2:
		mostrarOverlayArmaConseguidaSeleccionada2();
		break;
	case 3:
		mostrarOverlayArmaConseguidaSeleccionada3();
		break;
	}

}

bool PlayState::frameStarted(const Ogre::FrameEvent& evt) {
	Ogre::Real deltaT = evt.timeSinceLastFrame;

	_world->stepSimulation(deltaT, 1); // Actualizar simulacion Bullet

	//camara
	if (_exCamera) {
		_exCamera->update(deltaT, _robot->getCameraNode()->_getDerivedPosition(), _robot->getSightNode()->_getDerivedPosition());
	}

	checkOnGround();

	_robot->actuar(deltaT);
	int i;
	for (i = 0; i < (int) _robot->getEnemigos().size(); i++) {
		_robot->getEnemigos()[i]->actuar(deltaT);
	}

	if(_robot->conseguirObjeto(deltaT)){
		_diamantesConseguidos++;
	}

	_robot->conseguirPilaVida(deltaT);
	_robot->conseguirArma(deltaT);

	comprobarEnemigoFinal();
	comprobarDerrota();

	if (_allEnemiesDead)
		comprobarNivelTerminado();

	Ogre::OverlayElement *oe;
	oe = _overlayManager->getOverlayElement("Conseguidos");
	oe->setCaption(Ogre::StringConverter::toString(_diamantesConseguidos));
	oe = _overlayManager->getOverlayElement("Totales");
	oe->setCaption(Ogre::StringConverter::toString(_diamantesTotales));

	oe = _overlayManager->getOverlayElement("Vida");
	oe->setCaption(Ogre::StringConverter::toString(_robot->getVida()));

	oe = _overlayManager->getOverlayElement("Puntuacion");
	oe->setCaption(Ogre::StringConverter::toString(_robot->getPuntos()));

	return true;
}

bool PlayState::frameEnded(const Ogre::FrameEvent& evt) {
	if (_exitGame)
		return false;

	return true;
}

void PlayState::keyPressed(const OIS::KeyEvent &e) {
	// Tecla p --> PauseState.
	if (e.key == OIS::KC_P) {
		_armasConseguidas = _robot->getArmasConseguidas();
		pushState(PauseState::getSingletonPtr());
	}
	if (e.key == OIS::KC_UP) {
		_robot->setAvanzar(true);
	}
	if (e.key == OIS::KC_LEFT) {
		_robot->setGirarIzquierda(true);
	}
	if (e.key == OIS::KC_RIGHT) {
		_robot->setGirarDerecha(true);
	}
	if (e.key == OIS::KC_A) {
		if(_robot->getArmaSeleccionada() != 0)_robot->setEstaDisparando(true);
	}
	if (e.key == OIS::KC_SPACE) {
		_robot->setSaltar(true);
	}
	if (e.key == OIS::KC_1){
		_robot->setArmaSeleccionada(1);
		if(_robot->getArmasConseguidas()[0]) mostrarOverlayArmaConseguidaSeleccionada1();
	}
	if (e.key == OIS::KC_2){
		_robot->setArmaSeleccionada(2);
		if(_robot->getArmasConseguidas()[1]) mostrarOverlayArmaConseguidaSeleccionada2();
	}
	if (e.key == OIS::KC_3){
		_robot->setArmaSeleccionada(3);
		if(_robot->getArmasConseguidas()[2]) mostrarOverlayArmaConseguidaSeleccionada3();
	}

}

void PlayState::keyReleased(const OIS::KeyEvent &e) {
	if (e.key == OIS::KC_UP) {
		_robot->setAvanzar(false);
		_robot->pararAvance();
	}
	if (e.key == OIS::KC_LEFT) {
		_robot->setGirarIzquierda(false);
		_robot->pararGiro();
	}
	if (e.key == OIS::KC_RIGHT) {
		_robot->setGirarDerecha(false);
		_robot->pararGiro();
	}
}

void PlayState::mouseMoved(const OIS::MouseEvent &e) {
}

void PlayState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id) {
}

void PlayState::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id) {
}

PlayState* PlayState::getSingletonPtr() {
	return msSingleton;
}

PlayState& PlayState::getSingleton() {
	assert(msSingleton);
	return *msSingleton;
}

void PlayState::CreateInitialWorld() {
	crearTerreno();

	crearRobot(_scene->getCharacterData()->getPosition());

	//creamos los enemigos
	std::vector<EnemigoData*> enemigosData = _scene->getEnemigos();
	int i;
	for (i = 0; i < (int) enemigosData.size(); i++) {
		crearEnemigo(enemigosData[i]->getPosition(), enemigosData[i]->getType());
	}

	std::vector<DiamanteData*> diamantesData = _scene->getDiamantes();
	int j;
	for (j = 0; j < (int) diamantesData.size(); j++) {
		crearDiamantes(diamantesData[j]->getPosition());
	}

	std::vector<PilaVidaData*> pilaVidaData = _scene->getPilaVida();
	int h;
	for (h = 0; h < (int) pilaVidaData.size(); h++) {
		crearPilaVida(pilaVidaData[h]->getPosition());
	}

	std::vector<ObjetoArmaData*> objetoArmaData = _scene->getObjetoArmaData();
	int k;
	for (k = 0; k < (int) objetoArmaData.size(); k++) {
		crearObjetoArma(objetoArmaData[k]->getPosition());
	}
}

void PlayState::crearLuz() {
	//creamos un punto de luz
	Light* light = _sceneMgr->createLight("Light");
	light->setPosition(0, 150, 0);
	light->setDirection(0, 0, 0);
	light->setDiffuseColour(1, 1, 1);
	light->setSpecularColour(1, 1, 1);
	light->setType(Light::LT_POINT);
	light->setSpotlightFalloff(5.0f);
	light->setCastShadows(true);
}

void PlayState::crearRobot(Vector3 position) {
	btCollisionShape* robotShape = new btBoxShape(
			btVector3(1.7395, 3.6075, 1.384));
	btVector3 fallInertia(0, 0, 0);
	btVector3 btPosition(position.x, position.y + ROBOT_ALTURA / 2, position.z);

	//Creamos el nodo para el Robot
	SceneNode* nodeRobot = _sceneMgr->createSceneNode("Robot");
	Ogre::Entity* entRobot = _sceneMgr->createEntity("Robot", "Robot.mesh");
	entRobot->getMesh()->buildEdgeList();
	nodeRobot->attachObject(entRobot);
	_sceneMgr->getRootSceneNode()->addChild(nodeRobot);
	MyMotionState* robotMotionState = new MyMotionState(
			btTransform(btQuaternion(0, 0, 0, 1), btPosition), nodeRobot);
	btScalar robotMass = 1000;
	robotShape->calculateLocalInertia(robotMass, fallInertia);
	btRigidBody::btRigidBodyConstructionInfo robotRigidBodyCI(robotMass,
			robotMotionState, robotShape, fallInertia);
	btRigidBody* robotRigidBody = new btRigidBody(robotRigidBodyCI);
	robotRigidBody->setFriction(0);
	robotRigidBody->setRollingFriction(0);
	robotRigidBody->setAngularFactor(0);
	_world->addRigidBody(robotRigidBody);

	//nodo que nos servira para calcular la direccion al avanzar
	SceneNode* nodeRobotAvance = _sceneMgr->createSceneNode("RobotAvance");
	nodeRobot->addChild(nodeRobotAvance);
	nodeRobotAvance->translate(0.0, 0.0, 1.0);

	_robot = new Robot(nodeRobot, nodeRobotAvance, robotShape, robotRigidBody, _nivel);

	if(_armasConseguidas.size()){
		_robot->setArmasConseguidas(_armasConseguidas);
		mostrarOverlayArmasConseguidas();
	}

	if (_puntos > 0){
		_robot->setPuntos(_puntos);
	}
}

void PlayState::mostrarOverlayArmasConseguidas(){
	int i;
	for(i = 0; i < (int)_armasConseguidas.size(); i++){
		if (_armasConseguidas[i]) {
			std::stringstream saux;
			std::string s = "Arma";
			saux << s << (i+1);
			_overlayManager = Ogre::OverlayManager::getSingletonPtr();
			Ogre::Overlay *overlayArma = _overlayManager->getByName(saux.str());
			overlayArma->show();
		}
	}
}

void PlayState::crearEnemigo(Vector3 position, int type) {
	btVector3 btPosition(position.x, position.y, position.z);

	std::stringstream saux;
	std::string s = "Enemigo";
	saux << s << _contadorEnemigos;
	_contadorEnemigos++;

	Enemigo *enemigo;

	switch (type){
	case 1:
		enemigo = new Enemigo1(saux.str(), btPosition, _robot, _world);
		break;
	case 2:
		enemigo = new Enemigo2(saux.str(), btPosition, _robot, _world);
		break;
	case 3:
		enemigo = new Enemigo3(saux.str(), btPosition, _robot, _world);
		break;
	case 4:
		enemigo = new Enemigo4(saux.str(), btPosition, _robot, _world);
		break;
	}

	_robot->addEnemigo(enemigo);
}

void PlayState::crearTerreno() {
	btVector3 fallInertia(0, 0, 0);
	btVector3 btPosition(0.0, 0.0, 0.0);

	SceneNode* nodeTerreno;
	Entity* entTerreno;
	Ogre::MeshPtr MeshPtr;
	MeshStrider* Strider;


	//Creamos el nodo para el terreno
	switch(_nivel){
	case 1:
		nodeTerreno = _sceneMgr->createSceneNode("TerrenoNivel1");
		entTerreno = _sceneMgr->createEntity("TerrenoNivel1", "TerrenoNivel1.mesh");
		MeshPtr =	Ogre::Singleton<Ogre::MeshManager>::getSingletonPtr()->load("TerrenoNivel1.mesh", ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
		break;
	case 2:
		nodeTerreno = _sceneMgr->createSceneNode("TerrenoNivel2");
		entTerreno = _sceneMgr->createEntity("TerrenoNivel2", "TerrenoNivel2.mesh");
		MeshPtr =	Ogre::Singleton<Ogre::MeshManager>::getSingletonPtr()->load("TerrenoNivel2.mesh", ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
		break;
	case 3:
		nodeTerreno = _sceneMgr->createSceneNode("TerrenoNivel3");
		entTerreno = _sceneMgr->createEntity("TerrenoNivel3", "TerrenoNivel3.mesh");
		MeshPtr =	Ogre::Singleton<Ogre::MeshManager>::getSingletonPtr()->load("TerrenoNivel3.mesh", ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
		break;
	}

	nodeTerreno->attachObject(entTerreno);
	entTerreno->setCastShadows(false);
	entTerreno->getMesh()->buildEdgeList();
	_sceneMgr->getRootSceneNode()->addChild(nodeTerreno);

	Strider = new MeshStrider(MeshPtr.get());
	MeshPtr->buildEdgeList();

	btCollisionShape* terrenoShape = new btBvhTriangleMeshShape(Strider, true,
			true);

	MyMotionState* terrenoMotionState = new MyMotionState(
			btTransform(btQuaternion(0, 0, 0, 1), btPosition), nodeTerreno);
	btScalar terrenoMass = 0;
	terrenoShape->calculateLocalInertia(terrenoMass, fallInertia);
	btRigidBody::btRigidBodyConstructionInfo terrenoRigidBodyCI(terrenoMass,
			terrenoMotionState, terrenoShape, fallInertia);
	btRigidBody *terrenoRigidBody = new btRigidBody(terrenoRigidBodyCI);
	_world->addRigidBody(terrenoRigidBody);
}

void PlayState::crearDiamantes(Vector3 position){
	std::stringstream saux;
	std::string s = "Diamante";
	saux << s << _contadorDiamantes;

	SceneNode* nodeDiamante = _sceneMgr->createSceneNode(saux.str());
	Ogre::Entity* entDiamante = _sceneMgr->createEntity(saux.str(), "Diamante.mesh");
	entDiamante->getMesh()->buildEdgeList();
	nodeDiamante->attachObject(entDiamante);
	_sceneMgr->getRootSceneNode()->addChild(nodeDiamante);
	nodeDiamante->translate(position.x,position.y,position.z);

	_contadorDiamantes++;

	saux.str("");

	Diamante *diamante = new Diamante(nodeDiamante);
	_robot->addDiamante(diamante);
}

void PlayState::crearPilaVida(Vector3 position){
	std::stringstream saux;
	std::string s = "PilaVida";
	saux << s << _contadorPilaVida;

	SceneNode* nodePilaVida = _sceneMgr->createSceneNode(saux.str());
	Ogre::Entity* entPila = _sceneMgr->createEntity(saux.str(), "Pila.mesh");
	entPila->getMesh()->buildEdgeList();
	nodePilaVida->attachObject(entPila);
	_sceneMgr->getRootSceneNode()->addChild(nodePilaVida);
	nodePilaVida->translate(position.x,position.y,position.z);

	_contadorPilaVida++;

	saux.str("");

	PilaVida *pilaVida = new PilaVida(nodePilaVida);
	_robot->addPilaVida(pilaVida);
}

void PlayState::crearObjetoArma(Vector3 position){
	std::stringstream saux;
	std::string s = "ObjetoArma";
	saux << s << _contadorObjetoArma;

	SceneNode* nodeObjetoArma = _sceneMgr->createSceneNode(saux.str());
	Ogre::Entity* entObjetoArma = _sceneMgr->createEntity(saux.str(), "Gasolina.mesh");
	entObjetoArma->getMesh()->buildEdgeList();
	nodeObjetoArma->attachObject(entObjetoArma);
	_sceneMgr->getRootSceneNode()->addChild(nodeObjetoArma);
	nodeObjetoArma->translate(position.x,position.y,position.z);

	_contadorObjetoArma++;

	saux.str("");

	ObjetoArma *objetoArma = new ObjetoArma(nodeObjetoArma);
	_robot->addObjetoArma(objetoArma);
}

void PlayState::parsear_escenario() {
	Importer *importer = Importer::getSingletonPtr();
	_scene = new Scene;

	switch(_nivel){
	case 1:
		importer->parseScene("./data/nivel1.xml", _scene);
		break;
	case 2:
		importer->parseScene("./data/nivel2.xml", _scene);
		break;
	case 3:
		importer->parseScene("./data/nivel3.xml", _scene);
		break;
	}
}

void PlayState::checkOnGround() {
	Vector3 robotPosition = _robot->getNode()->getPosition();
	btVector3 start = btVector3(robotPosition.x, robotPosition.y, robotPosition.z);
	btVector3 end = btVector3(start.x(), start.y()-(ROBOT_ALTURA/2+0.1), start.z());
	btCollisionWorld::ClosestRayResultCallback RayCallback(start, end);

	// Perform raycast
	_world->rayTest(start, end, RayCallback);

	_robot->setOnGround(false);
	if(RayCallback.hasHit()) {
	    end = RayCallback.m_hitPointWorld;

	    _robot->setOnGround(true);
	}
}

int PlayState::getNivel(){
	return _nivel;
}

void PlayState::setNivel(int nivel){
	_nivel = nivel;
}

void PlayState::comprobarEnemigoFinal(){
	if (_diamantesConseguidos == _diamantesTotales && !_allEnemiesDead){
		_allEnemiesDead = true;
		enemigoFinal();
	}
}

void PlayState::enemigoFinal(){
	Vector3 position = _scene->getEnemigoFinal()->getPosition();
	btVector3 btPosition(position.x, position.y, position.z);

	int type = _scene->getEnemigoFinal()->getType();

	std::stringstream saux;
	std::string s = "Enemigo";
	saux << s << _contadorEnemigos;
	_contadorEnemigos++;

	switch(type){
	case 1:
		_enemigoFinal = new Enemigo1(saux.str(), btPosition, _robot, _world);
		break;
	case 2:
		_enemigoFinal = new Enemigo2(saux.str(), btPosition, _robot, _world);
		break;
	case 3:
		_enemigoFinal = new Enemigo3(saux.str(), btPosition, _robot, _world);
		break;
	case 4:
		_enemigoFinal = new Enemigo4(saux.str(), btPosition, _robot, _world);
		break;
	}

	_robot->addEnemigo(_enemigoFinal);
}

void PlayState::comprobarNivelTerminado(){
	if (_enemigoFinal && _enemigoFinal->isDead()){
		_armasConseguidas = _robot->getArmasConseguidas();
		_puntos = _robot->getPuntos();

		ElegirNivelState::getSingletonPtr()->setNivelConseguido(_nivel);
		if(_nivel == 3){
			changeState(WinState::getSingletonPtr());
		} else {
			changeState(NivelSuperadoState::getSingletonPtr());
		}
	}
}

void PlayState::comprobarDerrota(){
	if (_robot->hasPerdido()){
		changeState(LoseState::getSingletonPtr());
	}
}

void PlayState::setPuntos(int puntos){
	_puntos = puntos;
}

int PlayState::getPuntuacion(){
	return _robot->getPuntos();
}

void PlayState::mostrarOverlayArmaConseguidaSeleccionada1(){
	_overlayArmaSeleccionada1->show();
	_overlayArmaSeleccionada2->hide();
	_overlayArmaSeleccionada3->hide();
}

void PlayState::mostrarOverlayArmaConseguidaSeleccionada2(){
	_overlayArmaSeleccionada1->hide();
	_overlayArmaSeleccionada2->show();
	_overlayArmaSeleccionada3->hide();
}

void PlayState::mostrarOverlayArmaConseguidaSeleccionada3(){
	_overlayArmaSeleccionada1->hide();
	_overlayArmaSeleccionada2->hide();
	_overlayArmaSeleccionada3->show();
}
