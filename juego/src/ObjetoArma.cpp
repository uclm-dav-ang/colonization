#include <ObjetoArma.h>
#include <OgreSceneNode.h>

ObjetoArma::ObjetoArma(SceneNode* nodeArmaObjeto) {
	_nodeObjetoArma = nodeArmaObjeto;

	_animStateRotarObjetoArma = Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager")->getEntity(_nodeObjetoArma->getName())->getAnimationState("MoverGasolina");
	configurarAnimacion(_animStateRotarObjetoArma, true);
}

ObjetoArma::ObjetoArma(const ObjetoArma &obj){
	_nodeObjetoArma = obj.getNode();
}

ObjetoArma::~ObjetoArma() {
	delete _nodeObjetoArma;
}

ObjetoArma& ObjetoArma::operator= (const ObjetoArma &obj){
	delete _nodeObjetoArma;
	_nodeObjetoArma = obj._nodeObjetoArma;
	return *this;
}

SceneNode* ObjetoArma::getNode()const {
	return _nodeObjetoArma;
}

void ObjetoArma::ocultar() {
	_nodeObjetoArma->setVisible(false);
}

void ObjetoArma::animar(Ogre::Real deltaT){
	addTimeAnimacion(_animStateRotarObjetoArma, deltaT);
}

void ObjetoArma::configurarAnimacion(Ogre::AnimationState *animState, bool loop) {
	animState->setTimePosition(0.0);
	animState->setEnabled(true);
	animState->setLoop(loop);
}

void ObjetoArma::addTimeAnimacion(Ogre::AnimationState *animState, Ogre::Real deltaT) {
	animState->addTime(deltaT);
}

void ObjetoArma::pararAnimacion(Ogre::AnimationState *animState) {
	animState->setEnabled(false);
}
