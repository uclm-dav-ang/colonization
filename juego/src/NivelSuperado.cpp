#include "NivelSuperadoState.h"
#include "ElegirNivelState.h"
#include "PlayState.h"


template<> NivelSuperadoState* Ogre::Singleton<NivelSuperadoState>::msSingleton = 0;

void NivelSuperadoState::enter() {
	_root = Ogre::Root::getSingletonPtr();

	// Se recupera el gestor de escena y la cámara.
	_sceneMgr = _root->getSceneManager("SceneManager");
	_camera = _sceneMgr->getCamera("IntroCamera");
	_camera->setPosition(Ogre::Vector3(0.01, 20, 0));
	_camera->lookAt(Ogre::Vector3(0, 0, 0));
	_viewport = _root->getAutoCreatedWindow()->addViewport(_camera);


	_overlayManager = Ogre::OverlayManager::getSingletonPtr();
	Ogre::Overlay *overlay = _overlayManager->getByName("NivelSuperado");
	overlay->show();

	_exitGame = false;
}

void NivelSuperadoState::exit() {
	_root->getAutoCreatedWindow()->removeAllViewports();
	_sceneMgr->clearScene();

	Ogre::OverlayManager::getSingletonPtr()->getByName("NivelSuperado")->hide();
}

void NivelSuperadoState::pause() {
}

void NivelSuperadoState::resume() {
}

bool NivelSuperadoState::frameStarted(const Ogre::FrameEvent& evt) {

	return true;
}

bool NivelSuperadoState::frameEnded(const Ogre::FrameEvent& evt) {
	if (_exitGame)
		return false;

	return true;
}

void NivelSuperadoState::keyPressed(const OIS::KeyEvent &e) {
	// Tecla p --> Estado anterior.
	if (e.key == 28) {
		changeState(ElegirNivelState::getSingletonPtr());
	}
}

void NivelSuperadoState::keyReleased(const OIS::KeyEvent &e) {
}

void NivelSuperadoState::mouseMoved(const OIS::MouseEvent &e) {
}

void NivelSuperadoState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id) {
}

void NivelSuperadoState::mouseReleased(const OIS::MouseEvent &e,
		OIS::MouseButtonID id) {
}

NivelSuperadoState*
NivelSuperadoState::getSingletonPtr() {
	return msSingleton;
}

NivelSuperadoState&
NivelSuperadoState::getSingleton() {
	assert(msSingleton);
	return *msSingleton;
}
