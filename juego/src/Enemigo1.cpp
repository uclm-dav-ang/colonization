#include <Enemigo1.h>
#include <OgreSceneNode.h>
#include <cmath>

Enemigo1::Enemigo1(String nombre, btVector3 posicion, Robot *robot, btDiscreteDynamicsWorld* world) : Enemigo( nombre,  posicion, robot,  world) {
	_centro = ENEMIGO1Y/2;

	//nodos y rigidBody
	_EnemigoShape = new btBoxShape(btVector3(ENEMIGO1X/2, ENEMIGO1Y/2, ENEMIGO1Z/2));
	btVector3 fallInertia(0, 0, 0);

	btScalar enemigoMass = 10;

	_nodeEnemigo = _sceneMgr->createSceneNode(_mName);
	_entEnemigo = _sceneMgr->createEntity(_mName, "Alien.mesh");
	_nodeEnemigo->attachObject(_entEnemigo);
	_entEnemigo->getMesh()->buildEdgeList();
	_sceneMgr->getRootSceneNode()->addChild(_nodeEnemigo);
	MyMotionState* MotionStateEnemigo = new MyMotionState(btTransform(btQuaternion(0, 0, 0, 1), posicion), _nodeEnemigo);
	_EnemigoShape->calculateLocalInertia(enemigoMass, fallInertia);
	btRigidBody::btRigidBodyConstructionInfo EnemigoRigidBodyCI(enemigoMass, MotionStateEnemigo, _EnemigoShape, fallInertia);
	_EnemigoRigidBody = new btRigidBody(EnemigoRigidBodyCI);
	_EnemigoRigidBody->setRollingFriction(0);
	_EnemigoRigidBody->setFriction(0);
	_EnemigoRigidBody->setAngularFactor(0);
	_world->addRigidBody(_EnemigoRigidBody);

	stringstream nombreAvance;
	nombreAvance << _mName << "avance";

	_nodeEnemigoAvance = _sceneMgr->createSceneNode(nombreAvance.str());
	_nodeEnemigo->addChild(_nodeEnemigoAvance);
	_nodeEnemigoAvance->translate(0.0, 0.0, 1.0);


	_isDead = false;

	_estado_andar = true;
	_estado_atacar = false;
	_estado_perseguir = false;

	_giroIzquierda = true;

	_poder_ataque = 10;
	_vida = 1;
	_puntos = PUNTOS1;

	_contador = 1000;
	_comienzoContador = false;

	_animStateAndar = _entEnemigo->getAnimationState("Andar");
}

Enemigo1::Enemigo1(const Enemigo1 &obj) : Enemigo (obj){
	_nodeEnemigo = obj.getNode();
	_nodeEnemigoAvance = obj.getNodeAvance();
	_EnemigoShape = obj.getShape();
	_EnemigoRigidBody = obj.getRigidBody();
	_robot = obj.getRobot();
}

Enemigo1::~Enemigo1() {
	delete _nodeEnemigo;
	delete _nodeEnemigoAvance;
	delete _EnemigoShape;
	delete _EnemigoRigidBody;
}

Enemigo1& Enemigo1::operator= (const Enemigo1 &obj){
	delete _nodeEnemigo;
	delete _nodeEnemigoAvance;
	delete _EnemigoShape;
	delete _EnemigoRigidBody;
	_nodeEnemigo = obj._nodeEnemigo;
	_nodeEnemigo =obj._nodeEnemigoAvance;
	_EnemigoShape = obj._EnemigoShape;
	_EnemigoRigidBody = obj._EnemigoRigidBody;
	return *this;
}

void Enemigo1::actuar(Ogre::Real deltaT){
	if (_estado_andar) {
		andar(deltaT);
	} else if (_estado_perseguir) {
		perseguir(deltaT);
	} else {
		atacar();
	}

	if (_comienzoContador) {
		decrementarContador(deltaT);
	}

	if (_contador <= 0) {
		eliminarExplosion();
		deleteEnemigo();
	}
}

void Enemigo1::andar(Ogre::Real deltaT) {
	avanceAndar(deltaT);
	if(isDetected()) {
		_estado_andar = false;
		_estado_perseguir = true;
	}
}

void Enemigo1::perseguir(Ogre::Real deltaT) {
	if (!_animStateAndar->getEnabled()){
		configurarAnimacion(_animStateAndar, true);
	}

	if (_animStateAndar->getEnabled() && !_animStateAndar->hasEnded()){
		addTimeAnimacion(_animStateAndar, deltaT*4);
	}


	_EnemigoRigidBody->activate(true);
	btVector3 vectorRobot = calcularVectorRobot();
	vectorRobot = btVector3(vectorRobot.x(), 0.0, vectorRobot.z());
	calcularLinearVelocity();

	double angulo = calcularAngulo(_linearVelocity, vectorRobot);


	if (angulo > ANGULO_CON_ROBOT){
		btVector3 perpendicular = btVector3(vectorRobot.z(), vectorRobot.y(), -vectorRobot.x());
		double anguloConPerpendicular = calcularAngulo(_linearVelocity, perpendicular);
		if(anguloConPerpendicular >= 90){
			_giroIzquierda = true;
		} else {
			_giroIzquierda = false;
		}
		girar();
	} else {
		pararGiro();
		_EnemigoRigidBody->setLinearVelocity(_linearVelocity*4);
	}


	if (isNear()){
		_estado_atacar = true;
		_estado_perseguir = false;
	} else if (!isDetected()){
		_estado_andar = true;
		_estado_perseguir = false;
	}
}

void Enemigo1::atacar() {
	if (!isNear()) {
		_estado_atacar = false;
		_estado_perseguir = true;
	}

	if (!_isDead){
		morir();
		pararAvance();
		pararGiro();

		quitarVidaRobot();
	}
}

bool Enemigo1::isDetected() {
	bool isDetected = false;

	btVector3 vector = calcularVectorRobot();

	double distancia = calcularModulo(vector);

	if (5 < distancia && distancia < 40){
		isDetected = true;
	}

	return isDetected;
}

bool Enemigo1::isNear() {
	bool isNear = false;

	btVector3 vector = calcularVectorRobot();

	double distancia = calcularModulo(vector);

	if (distancia < 5) {
		isNear = true;
	}

	return isNear;
}

void Enemigo1::decrementarContador(Ogre::Real deltaT) {
	Ogre::Real deltaTime = _m_precisionTimer->getMilliseconds();
	_m_precisionTimer->reset();
	_contador -= deltaTime;
}

void Enemigo1::eliminarExplosion() {
	StringStream nombre;
	nombre << _nodeEnemigo->getName() << "particle";
	_sceneMgr->destroyParticleSystem(nombre.str());
	nombre.str("");
}

void Enemigo1::crearExplosion(){
	std::stringstream saux;
	std::string s = "particle";
	saux << _nodeEnemigo->getName() << s;

	Ogre::ParticleSystem* ps = _sceneMgr->createParticleSystem(saux.str(),"flame");
	Ogre::SceneNode* psNode = _sceneMgr->getRootSceneNode()->createChildSceneNode(saux.str());
	psNode->attachObject(ps);
	psNode->translate(_nodeEnemigo->getPosition().x, _nodeEnemigo->getPosition().y,_nodeEnemigo->getPosition().z);

	_gameManager->getSoundExplosion()->play();
}

void Enemigo1::morir() {
	_isDead = true;
	_comienzoContador = true;
	_m_precisionTimer = new Ogre::Timer();

	addPuntos(_puntos);

	crearExplosion();
	_world->removeRigidBody(_EnemigoRigidBody);
	ocultar();
}

void Enemigo1::restarVida(int danio) {
	_vida -= danio;

	if (_vida <= 0 && !isDead()){
		morir();
	}
}
