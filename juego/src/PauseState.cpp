#include "PauseState.h"
#include "IntroState.h"
#include "PlayState.h"

template<> PauseState* Ogre::Singleton<PauseState>::msSingleton = 0;

void PauseState::enter () {
  _root = Ogre::Root::getSingletonPtr();

  // Se recupera el gestor de escena y la cámara.
  _sceneMgr = _root->getSceneManager("SceneManager");

  if(!_sceneMgr->hasCamera("PauseCamera")){
	  _camera = _sceneMgr->createCamera("PauseCamera");
  }

  _camera->setPosition(Ogre::Vector3(0, 100, 0));
  _camera->lookAt(Ogre::Vector3(0.1, 0, 0));
  _camera->setNearClipDistance(1);
  _camera->setFarClipDistance(100);
  _camera->setFOVy(Ogre::Degree(50));
  _viewport = _root->getAutoCreatedWindow()->getViewport(0);
  _viewport->setCamera(_camera);


  _menuSeleccion = 0;
  crearMenu();
  cambiarFlecha();

  _animRotar = _sceneMgr->getEntity("Flecha")->getAnimationState("Moverse");
  _animRotar->setTimePosition(0.0);
  _animRotar->setEnabled(true);
  _animRotar->setLoop(true);

  _exitGame = false;
}

void PauseState::exit () {
}

void PauseState::pause () {
}

void PauseState::resume () {
}

bool PauseState::frameStarted (const Ogre::FrameEvent& evt) {
	Ogre::Real deltaT = evt.timeSinceLastFrame;

	if(_animRotar->getEnabled() && !_animRotar->hasEnded())
		_animRotar->addTime(deltaT);

	return true;
}

bool PauseState::frameEnded (const Ogre::FrameEvent& evt) {
  if (_exitGame)
    return false;
  
  return true;
}

void PauseState::keyPressed (const OIS::KeyEvent &e) {
  if (e.key == 28 && _menuSeleccion == 0) {
	  _sceneMgr->destroyEntity("Continuar");
	  _sceneMgr->destroySceneNode("Continuar");
	  _sceneMgr->destroyEntity("Salir");
	  _sceneMgr->destroySceneNode("Salir");
	  _sceneMgr->destroySceneNode("Menu");
	  _sceneMgr->destroyEntity("PlanoMenu");
	  _sceneMgr->destroySceneNode("PlanoMenu");
	  _sceneMgr->destroyEntity("Flecha");
	  _sceneMgr->destroySceneNode("Flecha");
	  popState();
  }

  if (e.key == 28 && _menuSeleccion == 1) {
	  _root->getAutoCreatedWindow()->removeAllViewports();
	  _sceneMgr->destroyAllCameras();
	  _sceneMgr->clearScene();
	  _root->destroySceneManager(_sceneMgr);

	  ocultarOverlays();
	  changeState(IntroState::getSingletonPtr());
  }

  if (e.key == OIS::KC_UP && _menuSeleccion > 0){
	  _menuSeleccion--;
	  cambiarFlecha();
  	 }

  if (e.key == OIS::KC_DOWN && _menuSeleccion < 1){
	  _menuSeleccion++;
	  _nodeFlecha->setPosition(-2.5, 85, -4);
	  cambiarFlecha();
  }
}

void PauseState::crearMenu(){
	Ogre::SceneNode *node_menu = _sceneMgr->createSceneNode("Menu");
	_sceneMgr->getRootSceneNode()->addChild(node_menu);

	//Nodo PlanoMenu
	Ogre::SceneNode *node_PlanoMenu = _sceneMgr->createSceneNode("PlanoMenu");
	Ogre::Entity* ent_PlanoMenu = _sceneMgr->createEntity("PlanoMenu", "PlanoMenu.mesh");
	node_PlanoMenu->attachObject(ent_PlanoMenu);
	node_menu->addChild(node_PlanoMenu);
	node_PlanoMenu->translate(0, 92, 0);
	node_PlanoMenu->yaw(Degree(-90));

	//Nodo Continuar
	Ogre::SceneNode *node_continuar = _sceneMgr->createSceneNode("Continuar");
	Ogre::Entity* ent_continuar = _sceneMgr->createEntity("Continuar", "Plane.mesh");
	node_continuar->attachObject(ent_continuar);
	node_menu->addChild(node_continuar);
	node_continuar->translate(2.5, 85, 0);
	node_continuar->yaw(Ogre::Degree(-90));
	node_continuar->scale(0.8, 1, 0.8);
	ent_continuar->setMaterialName("MaterialMenu6");

	//Nodo Salir
	Ogre::SceneNode *node_salir = _sceneMgr->createSceneNode("Salir");
	Ogre::Entity* ent_salir = _sceneMgr->createEntity("Salir", "Plane.mesh");
	node_salir->attachObject(ent_salir);
	node_menu->addChild(node_salir);
	node_salir->translate(-2.5, 85, 0);
	node_salir->yaw(Ogre::Degree(-90));
	node_salir->scale(0.8, 1, 0.8);
	ent_salir->setMaterialName("MaterialMenu5");

	//Nodo Flecha
	_nodeFlecha = _sceneMgr->createSceneNode("Flecha");
	Ogre::Entity* ent_flecha = _sceneMgr->createEntity("Flecha", "Flecha.mesh");
	_nodeFlecha->attachObject(ent_flecha);
	node_menu->addChild(_nodeFlecha);
	_nodeFlecha->translate(2.5, 85, -4);
	_nodeFlecha->yaw(Degree(-90));
	_nodeFlecha->setScale(0.5, 0.5, 0.5);
	_nodeFlecha->scale(4,4,4);
}

void PauseState::cambiarFlecha(){
	switch (_menuSeleccion){
	case 0:
		_nodeFlecha->setPosition(2.5, 85, -4);
		break;
	case 1:
		_nodeFlecha->setPosition(-2.5, 85, -4);
		break;
	}
}

void PauseState::ocultarOverlays(){
	Ogre::OverlayManager::getSingletonPtr()->getByName("VidaRobot")->hide();
	Ogre::OverlayManager::getSingletonPtr()->getByName("Objetos")->hide();
	Ogre::OverlayManager::getSingletonPtr()->getByName("MenuArmas")->hide();
	Ogre::OverlayManager::getSingletonPtr()->getByName("Arma1")->hide();
	Ogre::OverlayManager::getSingletonPtr()->getByName("Arma2")->hide();
	Ogre::OverlayManager::getSingletonPtr()->getByName("Arma3")->hide();
	Ogre::OverlayManager::getSingletonPtr()->getByName("ArmaSeleccionada1")->hide();
	Ogre::OverlayManager::getSingletonPtr()->getByName("ArmaSeleccionada2")->hide();
	Ogre::OverlayManager::getSingletonPtr()->getByName("ArmaSeleccionada3")->hide();
}

void PauseState::keyReleased (const OIS::KeyEvent &e) {
}

void PauseState::mouseMoved (const OIS::MouseEvent &e) {
}

void PauseState::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
}

void PauseState::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
}

PauseState* PauseState::getSingletonPtr () {
return msSingleton;
}

PauseState& PauseState::getSingleton () {
  assert(msSingleton);
  return *msSingleton;
}
