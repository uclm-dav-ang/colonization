#include <ObjetoArmaData.h>
#include <OgreSceneNode.h>
#include <cmath>

ObjetoArmaData::ObjetoArmaData
(Ogre::Vector3 position)
{
	_position = position;
}

ObjetoArmaData::~ObjetoArmaData ()
{
}

Vector3 ObjetoArmaData::getPosition()const {
	return _position;
}

void ObjetoArmaData::setPosition(Vector3 position){
	_position = position;
}
